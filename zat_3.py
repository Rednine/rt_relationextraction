from sklearn.ensemble import GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import classification_report, precision_recall_fscore_support
from sklearn.model_selection import GridSearchCV, train_test_split, StratifiedKFold
# from multiprocessing.dummy import Pool
from rwn_parser import *
from embedding_ops import *
from utils import *
from numpy import mean
from random import sample

# the purpose of this zero approximation test is to test the winning model from zat_1 and _2 at on distinguishing
# between different lexical relations
# the results are expected to be generally terrible
if __name__ == '__main__':

    log = Log()
    embedding_model = ModelWrapper(ModelTypes.W2V, "model.bin")

    syns = get_relations(RelTypes.SYN, "N", True)
    # ants = get_relations(RelTypes.ANT, "N", True)
    hyps = get_relations(RelTypes.HYP, "N", True)
    hprs = get_relations(RelTypes.HPR, "N", True)
    hyps.extend(hprs)
    neg_data = sample(hprs, len(syns))
    #neg_syns = get_negative_examples(len(syns), RelTypes.SYN, "N", True)
    # neg_ants = get_negative_examples(10000, RelTypes.ANT, "N", True)
    #neg_hyps = get_negative_examples(int(len(hyps) / 2), RelTypes.HPR, "N", True)
    #neg_hyps.extend(get_negative_examples(int(len(hyps) / 2), RelTypes.HYP, "N", True))

    #hyp_data = transform_data(hyps, neg_hyps, POSTypes.N, embedding_model, log)
    syn_data = transform_data(syns, neg_data, POSTypes.N, embedding_model, log)

    run_test_with_kfold(syn_data, GradientBoostingClassifier(learning_rate=0.1, max_depth=5), log, 3)