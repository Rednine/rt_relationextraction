from gensim.models import KeyedVectors
from gensim.models import Word2Vec
from gensim.models import fasttext
from gensim.test.utils import datapath
from enum import Enum
import numpy as np
from rwn_parser import pos_correspondences, RelTypes
from constants import ModelTypes, RelTypes, POSTypes, model_directory
from re import sub
import spacy
import torch
from camphr.pipelines.elmo import Elmo
from os import path as ospath
from pickle import load, dump
from deeppavlov.core.common.file import read_json
from deeppavlov import build_model, configs


class ModelWrapper:
    def __init__(self, mtype:Enum, model_name, ptype=None):

        self.keydict = {}
        self.name = model_name
        
        if mtype == ModelTypes.W2V:
            self.type = mtype
            ext = model_name.split(".")[-1]
            if ext == "bin":
                self.model = KeyedVectors.load_word2vec_format(model_directory + model_name, binary=True)
            elif ext == "vec":
                self.model = KeyedVectors.load_word2vec_format(model_directory + model_name, binary=False)
        if mtype == ModelTypes.ELMO:
            self.type = mtype
            self.dictpath = f"{model_directory}{self.name}keydict_{pos_correspondences[ptype][0]}.pkl"
            if ptype and ospath.exists(self.dictpath):
                with open(self.dictpath, "rb") as io:
                    self.keydict = load(io)
            nlp = spacy.blank("ru")
            elmo = Elmo.from_elmofiles(model_directory + model_name + "options.json", model_directory + model_name + "model.hdf5")
            nlp.add_pipe(elmo)

            self.model = nlp
        if mtype == ModelTypes.FT:
            self.type = mtype
            path = datapath(model_name)
            self.model = fasttext.load_facebook_vectors(path)
        if mtype == ModelTypes.BERT:
            bert_config = read_json(model_directory + model_name)
            # bert_config['metadata']['variables']['BERT_PATH'] = model_directory + model_name
            self.type = mtype
            self.model = build_model(bert_config)

    def get_vector_sequence(self, words: str):
        if self.keydict and words.lower() in self.keydict.keys():
            return self.keydict[words.lower()]
        if self.type == ModelTypes.ELMO:
            doc = self.model(words.lower())
            return doc.tensor
        else:
            raise NotImplementedError

    @staticmethod
    def get_diff_sequences(seq1, seq2, max_seq_len:int, embedding_dim:int, dual=False):
        if not dual:
            diff = np.concatenate([seq1, np.zeros((1, embedding_dim)), seq2])
            diff = np.concatenate([diff, np.zeros((max_seq_len - len(diff), embedding_dim))])
            return diff
        else:
            return [np.concatenate([seq1, np.zeros((max_seq_len - len(seq1), embedding_dim))]), np.concatenate([seq2, np.zeros((max_seq_len - len(seq2), embedding_dim))])]

    def get_vector(self, word: str, ptype:Enum):
        if self.keydict and word.lower() in self.keydict.keys():
            return self.keydict[word.lower()]
        if self.type == ModelTypes.W2V:
            try:
                word = sub(r'\([^)]*\)', '', word).strip()
                frags = [x.lower() for x in word.split()]

                if len(frags) == 1:
                    lookup = word.lower() + "_" + pos_correspondences[ptype][1]
                else:
                    lookup = "::".join(frags) + "_" + pos_correspondences[ptype][1]
                result = self.model[lookup]
                return result
            except KeyError:
                if len(frags) == 1:
                    return None
                else:
                    frag_vecs = [self.get_vector(frag, ptype) for frag in frags]
                    frag_vals = [frag is not None for frag in frag_vecs]
                    if not all(frag_vals):
                        return None
                    else:
                        current_vec = frag_vecs[0]
                        for i in range(1, len(frag_vecs)):
                            current_vec = np.add(current_vec, frag_vecs[i])

                        return current_vec
        elif self.type == ModelTypes.ELMO:
            doc = self.model(word.lower())
            vecs = doc.tensor
            if len(vecs) == 1:
                return vecs[0]
            current_vec = vecs[0]
            for i in range(1, len(vecs)):
                current_vec = np.add(current_vec, vecs[i]) / len(vecs)
            
            return current_vec
        elif self.type == ModelTypes.FT:
            return self.model.vw[word.lower()]
        elif self.type == ModelTypes.BERT:
            if type(word) == str:
                tokens, token_embs, subtokens, subtoken_embs, sent_max_embs, sent_mean_embs, bert_pooler_outputs = self.model([word])
            else:
                tokens, token_embs, subtokens, subtoken_embs, sent_max_embs, sent_mean_embs, bert_pooler_outputs = self.model(word)
            return list(sent_mean_embs)
        else:
            raise NotImplementedError

    def get_diff(self, vec1, vec2):
        if self.type in {ModelTypes.W2V, ModelTypes.ELMO, ModelTypes.FT}:
            return np.subtract(vec1, vec2)
        else:
            raise NotImplementedError

    def get_diff_from_words(self, word1: str, word2: str, ptype: Enum):
        v1 = self.get_vector(word1, ptype)
        v2 = self.get_vector(word2, ptype)
        if v1 is None or v2 is None:
            return None
        return self.get_diff(v1, v2)
    
    def closest(self, word):
        if self.type == ModelTypes.W2V:
            try:
                word = sub(r'\([^)]*\)', '', word).strip().lower()
                return self.model.most_similar(positive=[word])
            except:
                return []
        elif self.type == ModelTypes.ELMO:
            raise NotImplementedError
        else:
            raise NotImplementedError
            
    def generate_keydict(self, words, ptype):
        for word in words:
            self.keydict[word.lower()] = self.get_vector(word, ptype)
        with open(self.dictpath, "wb") as io:
            dump(self.keydict, io)

    def generate_keydict_seq(self, words):
        count = 1
        for word in words:
            self.keydict[word.lower()] = self.get_vector_sequence(word)
            print(f"Got {count} out of {len(words)}")
            count += 1
        with open(self.dictpath, "wb") as io:
            dump(self.keydict, io)
        
                


# def evaluate_vocab(embs:ModelWrapper, postype:Enum, rtype:Enum):
#     oov = set()
#     all_words = set()
#     data = get_relations(rtype, "N", False)
#     for parent, child in data:
#         pvec = embs.get_vector(parent, postype)
#         cvec = embs.get_vector(child, postype)
#         if pvec is None:
#             oov.add(parent)
#         if cvec is None:
#             oov.add(child)
#         all_words.add(parent)
#         all_words.add(child)
#     return oov, all_words


def evaluate_graph(embs:ModelWrapper, graph, postype:Enum):
    both_oov = 0
    both_iv = 0
    one_oov = 0
    total = 0
    for parent, child in graph.edges:
        pvec = embs.get_vector(parent, postype)
        cvec = embs.get_vector(child, postype)
        if pvec is not None and cvec is not None:
            both_iv += 1
        elif pvec is not None or cvec is not None:
            one_oov += 1
        else:
            both_oov += 1
        total += 1

    return both_iv, one_oov, both_oov, total


def find_worst_oov(embs: ModelWrapper, graph, postype:Enum):
    """
    finds the oov items with the largest amount of descendants i.e. contributing the most to total oov edges
    :param embs:
    :param graph:
    :param postype:
    :return:
    """
    res = []

    for parent in graph.nodes:
        pvec = embs.get_vector(parent, postype)
        if pvec is None:
            succ = graph.successors(parent)
            res.append((parent, len(list(succ))))

    return sorted(res, key=lambda x: x[1], reverse=True)
