# -*- coding: utf-8 -*-
from rwn_parser import transform_training_csvs_for_ranking, POSTypes
from embedding_ops import ModelWrapper, ModelTypes
from utils import mean_reciprocal_rank, build_zhang_gru
import numpy as np

model = build_zhang_gru()
model.load_weights("models/raters/zhang_gru_05012020_new_nr3.h5")
taiga = ModelWrapper(ModelTypes.W2V, "ruwikiruscorpora_upos_skipgram_300_2_2019.bin")

item_embs, answer_embs, items_to_answers, items_to_synsets, answers_to_synsets = transform_training_csvs_for_ranking("1_new", taiga, POSTypes.N)
possible_answers = list(answer_embs.keys())
possible_items = list(item_embs.keys())
pred_lists = []
count = 0
for i in range(250):
    count += 1
    true_item = possible_items[i]
    true_answers = items_to_answers[true_item]
    adiffs = [taiga.get_diff(answer_embs[x], item_embs[true_item]) for x in possible_answers]
    preds = list(np.squeeze(model.predict(np.array(adiffs))))
    sorted_answers = sorted(zip(possible_answers, preds), key=lambda x: x[1], reverse=True)
    pred_lists.append(list(dict.fromkeys([x[0] for x in sorted_answers if x[1] > 0.5])))
    print(f"Processed {count} out of {len(possible_items)}")
    
print("FINAL MRR IS:")
print(mean_reciprocal_rank(pred_lists, answers[:250]))
print("Done.")
