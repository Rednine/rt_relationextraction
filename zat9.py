from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import classification_report
from embedding_ops import ModelWrapper, ModelTypes
from utils import Log
from rwn_parser import POSTypes
import wandb
from pickle import dumps
from os import path


taiga = ModelWrapper(ModelTypes.W2V, "tayga_upos_skipgram_300_2_2019.bin")

X_train = []
y_train = []
X_val = []
y_val = []

if __name__ == '__main__':

    wandb.init(project="relationextraction")
    config = wandb.config
    config.lr = 0.1
    config.max_depth = 5
    config.max_features = "sqrt"
    config.n_estimators = 50
    config.min_samples_split = 0.7

    log = Log()
    train = []
    with open("resources/training_anlp.csv", encoding="utf-8") as io:
        count = 0
        lines = io.readlines()
        for line in lines:
            frags = line.split(",")
            if len(frags) != 5:
                continue
            parent = frags[0]
            child = frags[1]
            label = int(frags[4])
            diff = taiga.get_diff_from_words(parent, child, POSTypes.N)
            count += 1
            # print(f"Loaded {count} out of {len(lines)}")
            if diff is not None:
                X_train.append(diff)
                y_train.append(label)
    with open("resources/validation_anlp.csv", encoding="utf-8") as io:
        count = 0
        lines = io.readlines()
        for line in lines:
            frags = line.split(",")
            if len(frags) != 5:
                continue
            parent = frags[0]
            child = frags[1]
            label = int(frags[4])
            diff = taiga.get_diff_from_words(parent, child, POSTypes.N)
            count += 1
            # print(f"Loaded {count} out of {len(lines)}")

            if diff is not None:
                X_val.append(diff)
                y_val.append(label)
    print("Dataset loaded.")
    model = GradientBoostingClassifier(learning_rate=config.lr, max_depth=config.max_depth,
                                       max_features=config.max_features, verbose=2, n_estimators=config.n_estimators,
                                       min_samples_split=config.min_samples_split)

    model.fit(X_train, y_train)
    print("Model fitted.")
    preds = model.predict(X_val)
    classreport = classification_report(y_val, preds)
    log.write(classreport)
    wandb.log(classreport)
    dumps(model, path.join(wandb.run.dir, "model.pkl"))
    print("Done.")
