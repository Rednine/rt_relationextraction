from sklearn.ensemble import GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import classification_report, precision_recall_fscore_support
from sklearn.model_selection import GridSearchCV, train_test_split, StratifiedKFold
# from multiprocessing.dummy import Pool
from rwn_parser import *
from embedding_ops import *
from utils import *
from keras.models import Sequential
from keras.wrappers.scikit_learn import KerasClassifier
from keras.layers import Dropout, Dense, Input
from numpy import mean


# the purpose of this zero approximation test is to further test the winning model from zat_1
if __name__ == '__main__':

    log = Log()
    embedding_model = ModelWrapper(ModelTypes.W2V, "model.bin")

    syns = get_relations(RelTypes.SYN, "N", True)
    # ants = get_relations(RelTypes.ANT, "N", True)
    hyps = get_relations(RelTypes.HYP, "N", True)
    hprs = get_relations(RelTypes.HPR, "N", True)
    hyps.extend(hprs)
    neg_syns = get_negative_examples(len(syns), RelTypes.SYN, "N", True)
    # neg_ants = get_negative_examples(10000, RelTypes.ANT, "N", True)
    neg_hyps = get_negative_examples(int(len(hyps) / 2), RelTypes.HPR, "N", True)
    neg_hyps.extend(get_negative_examples(int(len(hyps) / 2), RelTypes.HYP, "N", True))

    hyp_data = transform_data(hyps, neg_hyps, POSTypes.N, embedding_model, log)
    syn_data = transform_data(syns, neg_syns, POSTypes.N, embedding_model, log)

    run_test_with_kfold(syn_data, get_classifier_nn(), log, 3)
    run_test_with_kfold(hyp_data, get_classifier_nn(), log, 3)
