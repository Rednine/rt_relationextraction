# -*- coding: utf-8 -*-
from rwn_parser import load_training_csvs_for_ranking, POSTypes
from embedding_ops import ModelWrapper, ModelTypes
from utils import mean_reciprocal_rank
from pandas import Series
from pickle import load

taiga = ModelWrapper(ModelTypes.W2V, "ruwikiruscorpora_upos_skipgram_300_2_2019.bin")
items, answers = load_training_csvs_for_ranking("0")
with open("models/raters/gb_glad_violet_27.pkl", "rb") as io: model = load(io)

answer_embs = [taiga.get_vector(x, POSTypes.N) for x in answers]
item_embs = [taiga.get_vector(x, POSTypes.N) for x in items]

none_answers = [x is not None for x in answer_embs]
none_items = [x is not None for x in item_embs]
both_present = [none_answers[i] and none_items[i] for i in range(len(items))]

answers = Series(answers)[both_present].tolist()
items = Series(items)[both_present].tolist()
answer_embs = Series(answer_embs)[both_present].tolist()
item_embs = Series(item_embs)[both_present].to_list()
aseries = Series(answers)

pred_lists = []
count = 0
for i in range(250):
    count += 1
    adiffs = [taiga.get_diff(x, item_embs[i]) for x in answer_embs]
    preds = [x[1] for x in model.predict_proba(adiffs)]
    sorted_answers = sorted(zip(answers, preds), key=lambda x: x[1], reverse=True)
    pred_lists.append(list(dict.fromkeys([x[0] for x in sorted_answers if x[1] > 0.5])))
    print(f"Processed {count} out of {len(items)}")

    
print("FINAL MRR IS:")
print(mean_reciprocal_rank(pred_lists, answers[:250]))
print("Done.")

with open("resources/example_answers_violet27.csv") as io:
    for i in range(len(pred_lists)):
        io.write(f"{repr(pred_lists[i])}\t{answers[i]}\n")
    

