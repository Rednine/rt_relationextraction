# -*- coding: utf-8 -*-
from rwn_parser import create_training_csvs
from constants import POSTypes

create_training_csvs("27042020_V_m0_n3_d3_wcorr-4", 3, postype=POSTypes.V, distance_setting=4, bert=True)
create_training_csvs("27042020_N_m0_n3_d3_wcorr-4", 3, postype=POSTypes.N, distance_setting=4, bert=True)

