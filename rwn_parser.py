from __future__ import division
from bs4 import BeautifulSoup
from itertools import combinations
from enum import Enum
from random import randint, choice, sample, choices
from itertools import accumulate
import networkx as nx
import numpy as np
from utils import *
import pandas as pd
from time import time
from constants import old_storage_directory, new_storage_directory, POSTypes, RelTypes, type_correspondences, \
    pos_correspondences, ModelTypes


def get_training_data_for_allennlp(core, leaves, graph, neg_ratio, max_children=None, distance_setting=1):
    core_pairs = []
    leaf_pairs = []
    core_list = list(core)
    leaf_list = list(leaves)
    node_data = graph.nodes(data="lemma")
    syn_data = graph.nodes(data="synset_id")
    paths = dict(nx.all_pairs_shortest_path_length(graph, cutoff=distance_setting))
    od = graph.out_degree
    weighted_core = list(accumulate([od[x] if od[x] else 1 for x in core_list]))

    for node in core:
        succs = get_successors(node, core, paths, max_n=max_children)
        [add_unit_to_x(core_pairs, paths, node, succ, node_data, syn_data, None) for succ in succs]
        print(f"Chose {len(core_pairs)} core candidates")
    length = len(core_pairs)
    get_inversions(core_pairs, int(length / 3) * neg_ratio)

    for i in range(int(length / 3) * neg_ratio):
        candidate1, candidate2 = get_negative(core_list, core_list, graph, weighted_core)
        core_pairs.append(create_row(node_data[candidate1], node_data[candidate2], candidate1, syn_data[candidate1],
                           candidate2, syn_data[candidate2], 0))
        print(f"Chose {len(core_pairs)} core candidates")

    synsets = collect_synsets_from_graph(graph)
    for i in range(int(length / 3) * neg_ratio):
        w1, w2 = get_synonym(synsets)
        core_pairs.append(create_row(node_data[w1], node_data[w2], w1, syn_data[w1], w2, syn_data[w2], 0))
        print(f"Chose {len(core_pairs)} core candidates")

    # GETTING TEST DATA FROM THIS POINT ONWARD
    for node in core:
        succs = get_successors(node, leaves, paths, max_n=max_children)
        [add_unit_to_x(leaf_pairs, paths, node, succ, node_data, syn_data, None) for succ in succs]
        print(f"Chose {len(leaf_pairs)} leaf candidates")
    length = len(leaf_pairs)

    get_inversions(leaf_pairs, int(length / 3) * neg_ratio)

    for i in range(int(length / 3) * neg_ratio):
        candidate1, candidate2 = get_negative(core_list, leaf_list, graph, weighted_core)
        add_unit_to_x(leaf_pairs, paths, candidate1, candidate2, node_data, syn_data, 0)
        print(f"Chose {len(leaf_pairs)} leaf candidates")

    for i in range(int(length / 3) * neg_ratio):
        w1, w2 = get_synonym(synsets)
        add_unit_to_x(leaf_pairs, paths, w1, w2, node_data, syn_data, 0)
        print(f"Chose {len(leaf_pairs)} leaf candidates")

    return pd.DataFrame(core_pairs), pd.DataFrame(leaf_pairs)


def get_inversions(pairs, length):
    samp = sample(pairs, length)
    inversions = [invert(row) for row in samp]
    pairs.extend(inversions)


def get_inversions_bert(pairs, length, lemmas):
    samp = sample(pairs, length)
    inversions = [invert_bert(row, lemmas) for row in samp]
    pairs.extend(inversions)


def invert(row_dict):
    new_rd = {"name1":row_dict["name2"], "name2":row_dict["name1"], "id1": row_dict["id2"], "synset1": row_dict["synset2"], "id2": row_dict["id1"], "synset2": row_dict["synset1"], "distance": 0}
    return new_rd


def invert_bert(rd, lemmas):
    new_rd = {"syn1":rd["syn2"], "syn2": rd["syn1"], "lemmas_1": lemmas[rd["syn2"]], "lemma_2": choice(lemmas[rd["syn1"]]), "distance": 0}
    return new_rd


def add_unit_to_x(pairs, paths, w1, w2, node_data, syn_data, distance) -> None:
    if distance is not None:
        pairs.append(create_row(node_data[w1], node_data[w2], w1, syn_data[w1], w2, syn_data[w2], distance))
    else:
        pairs.append(create_row(node_data[w1], node_data[w2], w1, syn_data[w1], w2, syn_data[w2], paths[w1][w2]))


def add_unit_to_x_bert(pairs, paths, syn1, syn2, syn1_lemmas, syn2_lemma, distance) -> None:
    if distance is not None:
        pairs.append(create_row_bert(syn1, syn2, syn1_lemmas, syn2_lemma, distance))
    else:
        pairs.append(create_row_bert(syn1, syn2, syn1_lemmas, syn2_lemma, paths[syn1][syn2]))


def create_row(name1, name2, id1, synset1, id2, synset2, distance) -> dict:
    return {"name1":name1, "name2":name2, "id1": id1, "synset1": synset1, "id2": id2, "synset2": synset2, "distance": distance}


def create_row_bert(syn1, syn2, lemmas_syn1, lemma_syn2, distance) -> dict:
    return {"syn1": syn1, "syn2": syn2, "lemmas_1": lemmas_syn1, "lemma_2": lemma_syn2, "distance": distance}


def weighted_choice(choices):
   total = sum(w for c, w in choices)
   r = random.uniform(0, total)
   upto = 0
   for c, w in choices:
      if upto + w >= r:
         return c
      upto += w
   assert False, "Shouldn't get here"


def normalize_weights(weights):
    sum_ = sum(weights)
    norm = [float(i) / sum_ for i in weights]
    return norm

def get_negatives_for_specific_nodes():
    pass


def get_successors(node, inset, paths, max_n=None) -> list:
        if not max_n:
            return [key for key in paths[node] if key in inset and key != node]
        else:
            return sample([key for key in paths[node] if key in inset and key != node], max_n)


def get_synonym(synsets):
    while True:
        synset = choice(list(synsets.values()))
        w1 = choice(synset)  # ["lemma"]
        w2 = choice(synset)  # ["lemma"]
        if w1 == w2: continue
        break
    return w1, w2


def get_synonym_bert(syn1, lemmas):
    return lemmas[syn1], choice(lemmas[syn1])


def get_negative(list1, list2, graph, weights=None):
    if not weights:
        while True:
            candidate1 = choice(list1)
            candidate2 = choice(list2)
            if candidate1 not in graph.successors(candidate2) and candidate2 not in graph.successors(candidate1):
                break
    else:
        while True:
            candidate1 = choices(list1, cum_weights=weights)[0]
            candidate2 = choice(list2)
            if candidate1 not in graph.successors(candidate2) and candidate2 not in graph.successors(candidate1):
                break
    return candidate1, candidate2


def get_training_data_for_bert(core, leaves, graph, neg_ratio, max_children, distance_setting):

    synset_data = graph.nodes(data="sense_lemmas")
    core_pairs = []
    leaf_pairs = []
    core_list = list(core)
    leaf_list = list(leaves)
    paths = dict(nx.all_pairs_shortest_path_length(graph, cutoff=distance_setting))
    od = graph.out_degree
    weighted_core = list(accumulate([od[x] if od[x] else 1 for x in core_list]))

    for node in core:
        succs = get_successors(node, core, paths, max_n=max_children)
        for succ in succs:
            succ_lemmas = synset_data[succ]
            [add_unit_to_x_bert(core_pairs, paths, node, succ, synset_data[node], succ_lemma, None) for succ_lemma in succ_lemmas]
            print(f"Chose {len(core_pairs)} core candidates")

    length = len(core_pairs)
    get_inversions_bert(core_pairs, int(length / 3) * neg_ratio, synset_data)

    for i in range(int(length / 3) * neg_ratio):
        candidate1, candidate2 = get_negative(core_list, core_list, graph, weighted_core)
        candidate2_lemmas = synset_data[candidate2]
        [add_unit_to_x_bert(core_pairs, paths, candidate1, candidate2, synset_data[candidate1], succ_lemma, 0) for succ_lemma in candidate2_lemmas]
        print(f"Chose {len(core_pairs)} core candidates")

    # synsets = collect_synsets_from_graph(graph)
    for i in range(int(length / 3) * neg_ratio):
        core_syn = choice(core_list)
        w1, w2 = get_synonym_bert(core_syn, synset_data)
        core_pairs.append(create_row_bert(core_syn, core_syn, w1, w2, 0))
        print(f"Chose {len(core_pairs)} core candidates")

    # GETTING TEST DATA FROM THIS POINT ONWARD
    for node in core:
        succs = get_successors(node, leaves, paths, max_n=max_children)
        for succ in succs:
            succ_lemmas = synset_data[succ]
            [add_unit_to_x_bert(leaf_pairs, paths, node, succ, synset_data[node], succ_lemma, None) for succ_lemma in
             succ_lemmas]
            print(f"Chose {len(core_pairs)} leaf candidates")
    length = len(leaf_pairs)

    get_inversions_bert(leaf_pairs, int(length / 3) * neg_ratio, synset_data)

    for i in range(int(length / 3) * neg_ratio):
        candidate1, candidate2 = get_negative(core_list, leaf_list, graph, weighted_core)
        candidate2_lemmas = synset_data[candidate2]
        [add_unit_to_x_bert(leaf_pairs, paths, candidate1, candidate2, synset_data[candidate1], succ_lemma, 0) for
         succ_lemma in candidate2_lemmas]
        print(f"Chose {len(leaf_pairs)} leaf candidates")

    for i in range(int(length / 3) * neg_ratio):
        leaf_syn = choice(leaf_list)
        w1, w2 = get_synonym_bert(leaf_syn, synset_data)
        core_pairs.append(create_row_bert(leaf_syn, leaf_syn, w1, w2, 0))
        print(f"Chose {len(leaf_pairs)} leaf candidates")

    return pd.DataFrame(core_pairs), pd.DataFrame(leaf_pairs)


def get_graph_based_training_data(data, graph, emb_model, postype: Enum):
    train_ret = []
    train_labels = []
    data_list = list(data)
    for node in data_list:
        for succ in graph.successors(node):  # lol succ
            if succ in data:
                vec = emb_model.get_diff_from_words(node, succ, postype)
                if vec is not None:
                    train_ret.append(vec)
                    train_labels.append(1)

    for i in range(int(len(train_labels) / 2)):
        got_rel = False
        while not got_rel:
            candidate1 = choice(data_list)
            candidate2 = choice(data_list)
            if candidate1 not in graph.successors(candidate2) and candidate2 not in graph.successors(candidate1):
                vec = emb_model.get_diff_from_words(candidate1, candidate2, postype)
                if vec is not None:
                    got_rel = True
                    train_ret.append(vec)
                    train_labels.append(0)

    synsets = collect_synsets_from_graph(graph)
    for i in range(int(len(train_labels) / 2)):
        got_rel = False
        while not got_rel:
            synset = choice(list(synsets.values()))
            w1 = choice(synset)  # ["lemma"]
            w2 = choice(synset)  # ["lemma"]
            if w1 == w2: continue
            vec = emb_model.get_diff_from_words(w1, w2, postype)
            if vec is not None:
                train_ret.append(vec)
                train_labels.append(0)
                got_rel = True
    return train_ret, train_labels


def collect_synsets_from_graph(graph: nx.DiGraph):
    synsets = dict()
    for node, attrs in graph.nodes(data=True):
        tag = attrs["synset_name"]
        synsets[tag] = synsets.get(tag, [])
        synsets[tag].append(node)
        # succ = graph.successors(node)
        # succ_synset_tags = set([graph.nodes(data=True)[x]["synset_name"] for x in succ])
        # for tag in succ_synset_tags:
        #     synset = [x for x in succ if x["synset_name"] == tag]
        #     synsets[tag] = synset

    return {k: v for k, v in synsets.items() if len(v) > 1}


def build_predict_graph(model, embs, graph: nx.DiGraph, data, postype):
    new_graph = nx.DiGraph()
    relationships = {}
    trains = list(data[0])
    tests = list(data[1])
    for n1 in trains:
        for n2 in graph.successors(n1):
            if n2 in data[0]:
                if not new_graph.has_node(n1):
                    new_graph.add_node(n1)
                if not new_graph.has_node(n2):
                    new_graph.add_node(n2)
                new_graph.add_edge(n2, n1)

    for j in range(len(tests)):
        child = tests[j]
        if not relationships.get(child, None):
            relationships[child] = []
        diffs = [embs.get_diff_from_words(x, child, postype) for x in trains]
        indices = [i for i in range(len(diffs)) if diffs[i] is not None]
        diffs = [x for x in diffs if x is not None]
        if len(diffs) == 0:
            continue
        # vec = embs.get_diff_from_words(parent, child, postype)
        pred = model.predict(diffs)
        proba = model.predict_proba(diffs)
        for i in range(len(indices)):
            if pred[i]:
                relationships[child].append((trains[indices[i]], proba[i, 1]))
        print(f"Processed {j} out of {len(tests)} test words")

    for key, value in relationships.items():
        if len(value) == 0:
            continue
        top_value = max(value, key=lambda x: x[1])
        new_graph.add_node(key)
        new_graph.add_edge(top_value, key)

    return new_graph, hits_at_k(graph, new_graph, data[0]), get_prediction_differences(graph, new_graph, data[1])


def build_ruthes_graph(postype: Enum, verbose: bool = False, new: bool = True, synset_graph=False) -> nx.DiGraph:
    storage_directory = old_storage_directory
    if new:
        storage_directory = new_storage_directory
    with open(storage_directory + "/" + "synset_relations." + pos_correspondences[postype][0] + ".xml",
              encoding="utf8") as synrel:
        with open(storage_directory + "/" + "synsets." + pos_correspondences[postype][0] + ".xml",
                  encoding="utf8") as synsets:
            with open(storage_directory + "/" + "senses." + pos_correspondences[postype][0] + ".xml",
                      encoding="utf8") as senses:
                soup_rel = BeautifulSoup(synrel, "lxml")
                soup_syn = BeautifulSoup(synsets, "lxml")
                soup_sense = BeautifulSoup(senses, "lxml")
                synset_list = soup_syn.find_all("synset")
                synset_dict = {}
                for syn in synset_list:
                    synset_dict[syn["id"]] = syn
                sense_list = soup_sense.find_all("sense")
                sense_dict = {}
                for sense in sense_list:
                    sense_dict[sense["id"]] = sense
                graph = nx.DiGraph()
                rel_list = soup_rel.find_all("relation")

                count = 1
                for rel in rel_list:
                    if rel["name"] == "hypernym" or rel["name"] == "instance hypernym":
                        synset1 = synset_dict[rel["parent_id"]]
                        synset2 = synset_dict[rel["child_id"]]
                        senses1 = synset1.find_all("sense")
                        senses2 = synset2.find_all("sense")
                        if not synset_graph:
                            for sense1 in senses1:
                                for sense2 in senses2:
                                    # node1 = RuThesNode(rel["parent_id"], synset1["ruthes_name"])
                                    # node2 = RuThesNode(rel["child_id"], synset2["ruthes_name"])
                                    n1 = sense1["id"]
                                    n2 = sense2["id"]
                                    n1_lemma = sense_dict[sense1["id"]]["lemma"]  # (count * 2) - 1
                                    n2_lemma = sense_dict[sense2["id"]]["lemma"]  # (count * 2)
                                    if not graph.has_node(n1):
                                        graph.add_node(n1, synset_name=synset1["ruthes_name"],
                                                       synset_id=rel["parent_id"], lemma=n1_lemma.lower())
                                    if not graph.has_node(n2):
                                        graph.add_node(n2, synset_name=synset2["ruthes_name"],
                                                       synset_id=rel["child_id"], lemma=n2_lemma.lower())
                                    graph.add_edge(n2, n1)
                        else:
                            n1 = rel["parent_id"]
                            n2 = rel["child_id"]
                            sense_ids1 = [sense_dict[s["id"]]["lemma"].lower() for s in senses1]
                            sense_ids2 = [sense_dict[s["id"]]["lemma"].lower() for s in senses2]
                            if not graph.has_node(n1):
                                graph.add_node(n1, synset_name=synset1["ruthes_name"], sense_lemmas=sense_ids1)
                            if not graph.has_node(n2):
                                graph.add_node(n2, synset_name=synset2["ruthes_name"], sense_lemmas=sense_ids2)
                            graph.add_edge(n2, n1)

                    count += 1
                    if verbose:
                        print(f"Processed {count} out of {len(rel_list)}")

    return graph


def build_ruthes_graph_synsets(postype: Enum, verbose: bool = False, new: bool = True) -> nx.DiGraph:
    storage_directory = old_storage_directory
    if new:
        storage_directory = new_storage_directory
    with open(storage_directory + "/" + "synset_relations." + pos_correspondences[postype][0] + ".xml",
              encoding="utf8") as synrel:
        with open(storage_directory + "/" + "synsets." + pos_correspondences[postype][0] + ".xml",
                  encoding="utf8") as synsets:
            with open(storage_directory + "/" + "senses." + pos_correspondences[postype][0] + ".xml",
                      encoding="utf8") as senses:
                soup_rel = BeautifulSoup(synrel, "lxml")
                soup_syn = BeautifulSoup(synsets, "lxml")
                soup_sense = BeautifulSoup(senses, "lxml")
                synset_list = soup_syn.find_all("synset")
                synset_dict = {}
                for syn in synset_list:
                    synset_dict[syn["id"]] = syn
                sense_list = soup_sense.find_all("sense")
                sense_dict = {}
                for sense in sense_list:
                    sense_dict[sense["id"]] = sense
                graph = nx.DiGraph()
                rel_list = soup_rel.find_all("relation")

                count = 1
                for rel in rel_list:
                    if rel["name"] == "hypernym" or rel["name"] == "instance hypernym":
                        synset1 = synset_dict[rel["parent_id"]]
                        synset2 = synset_dict[rel["child_id"]]
                        senses1 = synset1.find_all("sense")
                        senses2 = synset2.find_all("sense")
                        for sense1 in senses1:
                            for sense2 in senses2:
                                # node1 = RuThesNode(rel["parent_id"], synset1["ruthes_name"])
                                # node2 = RuThesNode(rel["child_id"], synset2["ruthes_name"])
                                n1 = sense1["id"]
                                n2 = sense2["id"]
                                n1_lemma = sense_dict[sense1["id"]]["lemma"]  # (count * 2) - 1
                                n2_lemma = sense_dict[sense2["id"]]["lemma"]  # (count * 2)
                                if not graph.has_node(n1):
                                    graph.add_node(n1, synset_name=synset1["ruthes_name"], synset_id=rel["parent_id"],
                                                   lemma=n1_lemma)
                                if not graph.has_node(n2):
                                    graph.add_node(n2, synset_name=synset2["ruthes_name"], synset_id=rel["child_id"],
                                                   lemma=n2_lemma)
                                graph.add_edge(n2, n1)

                    count += 1
                    if verbose:
                        print(f"Processed {count} out of {len(rel_list)}")

    return graph


def create_training_csvs(suffix: str, neg_ratio: int, bert=False, postype=POSTypes.N, max_children=None, distance_setting=1):
    graph = build_ruthes_graph(postype, synset_graph=bert, verbose=False)
    core, leaves = create_principled_training_data_leafcut(graph, 0.2)
    if not bert:
        cp, lp = get_training_data_for_allennlp(core, leaves, graph, neg_ratio, max_children, distance_setting)
        cp.to_csv(f"resources/training_{suffix}.csv", sep=",", header=False, index=False)
        lp.to_csv(f"resources/validation_{suffix}.csv", sep=",", header=False, index=False)

    else:
        cp, lp = get_training_data_for_bert(core, leaves, graph, neg_ratio, max_children, distance_setting)
        cp.to_csv(f"resources/bert_training_{suffix}.csv", sep="\t", header=False, index=False)
        lp.to_csv(f"resources/bert_validation_{suffix}.csv", sep="\t", header=False, index=False)


def create_training_jsonl(neg_ratio: int):
    graph = build_ruthes_graph(POSTypes.N, synset_graph=True)
    core, leaves = create_principled_training_data_leafcut(graph, 0.2)
    cp, lp = get_training_data_for_bert(core, leaves, graph, neg_ratio)
    with open(f"resources/train.jsonl", "w", encoding="utf-8") as io:
        for parent, child, clemma, lemmas, label in cp:
            obj = repr([f"{clemma.lower()}: {'|'.join([x.lower() for x in lemmas])}",
                        {"cats": {"0": float(int(label) == 0), "1": float(int(label) == 1)}}]).replace("\'", "\"")
            io.write(f"{obj}\n")
        for parent, child, clemma, lemmas, label in lp:
            obj = repr([f"{clemma.lower()}: {'|'.join([x.lower() for x in lemmas])}",
                        {"cats": {"0": float(int(label) == 0), "1": float(int(label) == 1)}}]).replace("\'", "\"")
            io.write(f"{obj}\n")

    with open(f"resources/label.json", "w", encoding="utf-8") as io:
        io.write('["0", "1"]')


def load_training_csvs_pandas(suffix: str, embs, postype):
    ctime = time()
    print("Started loading training CSV")
    table_tr = pd.read_csv(f"resources/training_{suffix}.csv", sep=",")
    table_tr["vectors"] = table_tr.apply(lambda row: embs.get_diff_from_words(row["name1"], row["name2"], postype), axis="columns")
    table_val = pd.read_csv(f"resources/validation_{suffix}.csv", sep=",")
    table_val["vectors"] = table_val.apply(lambda row: embs.get_diff_from_words(row["name1"], row["name2"], postype), axis="columns")
    ftime = time()
    print(f"Loaded, time elapsed: {ftime - ctime}")
    return table_tr["vectors"].values, table_val["vectors"].values, table_tr["distance"].values, table_val["distance"].values


def load_training_csvs(suffix: str, embs, postype, verbose=False, max_count=None):
    ctime = time()
    print("Started loading training CSV")
    X_train, X_val, y_train, y_val = [], [], [], []
    with open(f"resources/training_{suffix}.csv", encoding="utf-8") as io:
        count = 0
        lines = io.readlines()
        if max_count:
            lines = sample(lines, max_count)
        for line in lines:
            frags = line.split(",")
            if len(frags) != 7:
                continue
            parent = frags[0]
            child = frags[1]
            label = int(int(frags[-1]) != 0)
            diff = embs.get_diff_from_words(parent, child, postype)
            count += 1
            if verbose:
                print(f"Loaded {count} out of {len(lines)}")
            if diff is not None:
                X_train.append(diff)
                y_train.append(label)
    with open(f"resources/validation_{suffix}.csv", encoding="utf-8") as io:
        count = 0
        lines = io.readlines()
        if max_count:
            lines = sample(lines, int(max_count / 2))
        for line in lines:
            frags = line.split(",")
            if len(frags) != 7:
                continue
            parent = frags[0]
            child = frags[1]
            label = int(int(frags[-1]) != 0)
            diff = embs.get_diff_from_words(parent, child, postype)
            count += 1

            if verbose:
                print(f"Loaded {count} out of {len(lines)}")

            if diff is not None:
                X_val.append(diff)
                y_val.append(label)

    ftime = time()
    print(f"Loaded, time elapsed: {ftime - ctime}")
    return X_train, X_val, y_train, y_val


class TrainingGeneratorRec:
    def __init__(self, path, embs, embs_dim):
        if type(path) is str:
            with open(f"resources/{path}") as io:
                self.lines = io.readlines()
        elif type(path) is list:
            self.lines = []
            for i in path:
                with open(f"resources/{i}") as io:
                    self.lines.extend(io.readlines())
        self.embs = embs
        self.embedding_dim = embs_dim

    def generate(self, max_sequence_length, batch_size, dual_inputs=False):
            while True:
                shuffle(self.lines)
                count = 0
                for i in range(int(len(self.lines)/batch_size)):
                    if (len(self.lines) - count) <= batch_size:
                        break
                    current_lines = self.lines[count:count + batch_size]
                    count += batch_size
                    current_X = []
                    parent_X = []
                    child_X = []
                    current_y = []
                    for line in current_lines:
                        frags = line.split(",")
                        parent = frags[0]
                        child = frags[1]
                        label = int(int(frags[-1]) != 0)
                        parent_emb = self.embs.get_vector_sequence(parent)
                        child_emb = self.embs.get_vector_sequence(child)
                        if not dual_inputs:
                            diff = np.concatenate([parent_emb, np.zeros((1, self.embedding_dim)), child_emb])
                            diff = np.concatenate([diff, np.zeros((max_sequence_length - len(diff), self.embedding_dim))])
                            count += 1
                            current_X.append(diff)
                            current_y.append(label)
                        else:
                            parent_emb = np.concatenate([parent_emb, np.zeros((max_sequence_length - len(parent_emb), self.embedding_dim))])
                            child_emb = np.concatenate([child_emb, np.zeros((max_sequence_length - len(child_emb), self.embedding_dim))])
                            parent_X.append(parent_emb)
                            child_X.append(child_emb)
                            current_y.append(label)
                    if not dual_inputs:
                        yield np.stack(current_X, axis=0), current_y
                    else:
                        yield [np.stack(parent_X, axis=0), np.stack(child_X, axis=0)], current_y

    def get_sample(self, sample_size, max_sequence_length, dual_inputs=False):
        shuffle(self.lines)
        current_lines = self.lines[:sample_size]
        current_X, current_y, parent_X, child_X = [], [], [], []
        for line in current_lines:
            frags = line.split(",")
            parent = frags[0]
            child = frags[1]
            label = int(int(frags[-1]) != 0)
            parent_emb = self.embs.get_vector_sequence(parent)
            child_emb = self.embs.get_vector_sequence(child)
            if not dual_inputs:
                diff = np.concatenate([parent_emb, np.zeros((1, self.embedding_dim)), child_emb])
                diff = np.concatenate([diff, np.zeros((max_sequence_length - len(diff), self.embedding_dim))])
                current_X.append(diff)
            else:
                parent_emb = np.concatenate(
                    [parent_emb, np.zeros((max_sequence_length - len(parent_emb), self.embedding_dim))])
                child_emb = np.concatenate(
                    [child_emb, np.zeros((max_sequence_length - len(child_emb), self.embedding_dim))])
                parent_X.append(parent_emb)
                child_X.append(child_emb)
            current_y.append(label)
        return [parent_X, child_X], current_y


def load_training_csvs_rec(suffix: str, embs, max_sequence_length, embedding_dim, verbose=False, max_count=None):
    ctime = time()
    print("Started loading training CSV")
    X_train, X_val, y_train, y_val = [], [], [], []
    with open(f"resources/training_{suffix}.csv", encoding="utf-8") as io:
        count = 0
        lines = io.readlines()
        if max_count:
            lines = sample(lines, max_count)
        for line in lines:
            frags = line.split(",")
            if len(frags) != 7:
                continue
            parent = frags[0]
            child = frags[1]
            label = int(int(frags[-1]) != 0)
            parent_emb = embs.get_vector_sequence(parent)
            child_emb = embs.get_vector_sequence(child)
            diff = np.concatenate([parent_emb, np.zeros((1, embedding_dim)), child_emb])
            diff = np.concatenate([diff, np.zeros((max_sequence_length - len(diff), embedding_dim))])
            count += 1
            if verbose:
                print(f"Loaded {count} out of {len(lines)}")
            if diff is not None:
                X_train.append(diff)
                y_train.append(label)
    with open(f"resources/validation_{suffix}.csv", encoding="utf-8") as io:
        count = 0
        lines = io.readlines()
        if max_count:
            lines = sample(lines, int(max_count / 2))
        for line in lines:
            frags = line.split(",")
            if len(frags) != 7:
                continue
            parent = frags[0]
            child = frags[1]
            label = int(int(frags[-1]) != 0)
            parent_emb = embs.get_vector_sequence(parent)
            child_emb = embs.get_vector_sequence(child)
            diff = np.concatenate([parent_emb, np.zeros((1, embedding_dim)), child_emb])
            diff = np.concatenate([diff, np.zeros((max_sequence_length - len(diff), embedding_dim))])
            count += 1

            if verbose:
                print(f"Loaded {count} out of {len(lines)}")

            if diff is not None:
                X_val.append(diff)
                y_val.append(label)

    ftime = time()
    print(f"Loaded, time elapsed: {ftime - ctime}")
    return X_train, X_val, y_train, y_val

def process_bert_line(line):
    frags = line.split("\t")

    parent = ", ".join(eval(frags[2]))
    child = frags[3]
    label = int(int(frags[-1]) != 0)
    text = parent + "|" + child
    return text, label


def load_training_csvs_bert(suffix: str, embs, postype, verbose=False, max_count=None, batch_size=1):
    ctime = time()
    print("Started loading training CSV")
    X_train, X_val, y_train, y_val = [], [], [], []
    with open(f"resources/bert_training_{suffix}.csv", encoding="utf-8") as io:
        lines = io.readlines()
        if max_count:
            lines = sample(lines, max_count)
        start_i = 0
        temp_lines = []
        while start_i < len(lines):
            for i in range(min(batch_size, len(lines) - start_i)):
                c_i = start_i + i
                ctext, clabel = process_bert_line(lines[c_i])
                y_train.append(clabel)
                temp_lines.append(ctext)
            else:
                cembs = embs.get_vector(temp_lines, postype)
                X_train.extend(cembs)
                start_i += batch_size
                temp_lines = []
                if verbose:
                    print(f"Processed {start_i} lines out of {len(lines)}")

    with open(f"resources/bert_validation_{suffix}.csv", encoding="utf-8") as io:
        lines = io.readlines()
        if max_count:
            lines = sample(lines, max_count)
        start_i = 0
        temp_lines = []
        while start_i < len(lines):
            for i in range(min(batch_size, len(lines) - start_i)):
                c_i = start_i + i
                ctext, clabel = process_bert_line(lines[c_i])
                y_val.append(clabel)
                temp_lines.append(ctext)
            else:
                cembs = embs.get_vector(temp_lines, postype)
                X_val.extend(cembs)
                start_i += batch_size
                temp_lines = []
                if verbose:
                    print(f"Processed {start_i} lines out of {len(lines)}")

    ftime = time()
    print(f"Loaded, time elapsed: {ftime - ctime}")
    return X_train, X_val, y_train, y_val


def load_training_csvs_for_ranking(suffix: str, get_syn=True, verbose=False):
    items, answers = [], []
    if get_syn:
        psyn, csyn = [], []
    with open(f"resources/validation_{suffix}.csv", encoding="utf-8") as io:
        lines = io.readlines()
        count = 0
        for line in lines:

            frags = line.split(",")
            if len(frags) != 7:
                continue
            parent = frags[0]
            child = frags[1]
            if get_syn:
                parent_synset = frags[3]
                child_synset = frags[5]
            label = int(frags[-1])
            count += 1
            if verbose:
                print(f"Loaded {count} out of {len(lines)}")

            if label:
                items.append(child)
                answers.append(parent)
                if get_syn:
                    psyn.append(parent_synset)
                    csyn.append(child_synset)
    if get_syn:
        return items, answers, psyn, csyn
    else:
        return items, answers


def load_synsets_for_ranking(postype: POSTypes, embs, graph):
    if embs.type == ModelTypes.ELMO:
        syn_names = graph.nodes(data="synset_id")
        lemmas = graph.nodes(data="lemma")
        # answer_embs = embs.keydict
        # assert len(answer_embs) == len(graph)
        answer_embs = {lemmas[x]: embs.get_vector(lemmas[x], postype) for x in graph}
        answers_to_synsets = {lemmas[node]: syn_names[node] for node in graph}  # or whatever
    return answer_embs, answers_to_synsets


def load_synsets_for_ranking_seq(embs, graph):
    if embs.type == ModelTypes.ELMO:
        syn_names = graph.nodes(data="synset_id")
        lemmas = graph.nodes(data="lemma")
        # answer_embs = embs.keydict
        # assert len(answer_embs) == len(graph)
        answer_embs = {lemmas[x]: embs.get_vector_sequence(lemmas[x]) for x in graph}
        answers_to_synsets = {lemmas[node]: syn_names[node] for node in graph}  # or whatever
    return answer_embs, answers_to_synsets


def transform_training_csvs_for_ranking(suffix: str, embs, postype: POSTypes):
    items, answers, psyn, csyn = load_training_csvs_for_ranking(suffix)
    items_to_answers = {}
    items_to_synsets = {}
    answers_to_synsets = {}

    answer_set = set(answers)
    item_set = set(items)
    answer_embs = {x: embs.get_vector(x, postype) for x in list(answer_set)}
    item_embs = {x: embs.get_vector(x, postype) for x in list(item_set)}
    answer_embs = {x[0]: x[1] for x in answer_embs.items() if x[1] is not None}
    item_embs = {x[0]: x[1] for x in item_embs.items() if x[1] is not None}

    # all_possible_items = set(item_embs.keys())
    # all_possible_answers = set(answer_embs.keys())

    for i in range(len(items)):
        if not items_to_answers.get(items[i]):
            items_to_answers[items[i]] = []
        items_to_answers[items[i]].append(answers[i])

    for i in range(len(items)):
        if not items_to_synsets.get(items[i]):
            items_to_synsets[items[i]] = csyn[i]

    for i in range(len(answers)):
        if not answers_to_synsets.get(answers[i]):
            answers_to_synsets[answers[i]] = psyn[i]

    return item_embs, answer_embs, items_to_answers, items_to_synsets, answers_to_synsets


def load_id_to_synset_name(postype):
    with open(new_storage_directory + "/" + "synsets." + pos_correspondences[postype][0] + ".xml",
              encoding="utf8") as synsets:
        soup_syn = BeautifulSoup(synsets, "lxml")
        synset_list = soup_syn.find_all("synset")
        synset_dict = {}
        for syn in synset_list:
            synset_dict[syn["id"]] = syn["ruthes_name"]

        return synset_dict


def get_parents_by_lemma(word, graph):
    wordnodes = [x for x, y in graph.nodes(data=True) if y['lemma'] == word]
    parents = []
    for wordnode in wordnodes:
        pass


def process_wordlist_seq(words, taiga, model, filename, postype, max_seq_len, embedding_dim,
                     filter_func=lambda items, graph, entries: [(x[0], x[1]) for x in items[:entries]], max_entries=10, graph_has_answers=False, dual=False):

    graph = build_ruthes_graph(postype, synset_graph=False)
    word_embs = [taiga.get_vector_sequence(x) for x in words]
    answer_embs, answers_to_synsets = load_synsets_for_ranking_seq(taiga, graph)
    answer_embs = {x.lower(): y for x, y in answer_embs.items()}
    answers_to_synsets = {x.lower(): y for x, y in answers_to_synsets.items() if x is not None}
    possible_answers = list(answer_embs.keys())

    print(f"total words: {len(words)}, words not none: {sum([x is not None for x in word_embs])}")

    results = []
    id_name_dict = load_id_to_synset_name(postype)
    sgraph = build_ruthes_graph(postype, synset_graph=True)
    for i in range(len(words)):
        if word_embs[i] is None:
            continue
        else:
            if not dual:
                adiffs = [taiga.get_diff_sequences(answer_embs[x], word_embs[i], max_seq_len, embedding_dim, dual) for x
                          in possible_answers]
                preds = list(np.squeeze(model.predict(np.array(adiffs))))
            else:
                adiffs = [taiga.get_diff_sequences(answer_embs[x], word_embs[i], max_seq_len, embedding_dim, dual) for x
                          in possible_answers]
                adiffs = [[x[0] for x in adiffs], [x[1] for x in adiffs]]
                preds = list(np.squeeze(model.predict(adiffs)))

            sorted_answers = sorted(zip(possible_answers, preds), key=lambda x: x[1], reverse=True)
            closest = get_synset_rankings_nomean(sorted_answers, answers_to_synsets)
            filtered_rankings = filter_func(sorted(closest.items(), key=lambda x: x[1], reverse=True), sgraph,
                                            max_entries)
            for synset, score in filtered_rankings:
                results.append((words[i], synset, id_name_dict[synset]))

        print(f"processed {i} out of {len(words)}")
    if not graph_has_answers:
        with open(f"results/{filename}.tsv", "w") as io:
            for word, synset, name in results:
                io.write(f"{word}\t{synset}\t{name}\n")
    else:
        with open(f"results/{filename}.tsv", "w") as io:
            # sgraph = build_ruthes_graph(postype, synset_graph=True)
            roots = get_root_nodes(sgraph)

            for word, synset, name in results:
                io.write(f"{word}\t{synset}\t{name}\t{get_distance_from_synset_to_word(sgraph, answers_to_synsets[word], synset)}\t{get_distance_from_root_synsets(sgraph, synset, roots)}\n")

    with open(f"results/{filename}_nonames.tsv", "w") as io:
        for word, synset, name in results:
            io.write(f"{word}\t{synset}\n")


def load_bert_data_for_ranking(lemmas, embs, word, ptype):
    return [x[0] for x in [embs.get_vector(", ".join(lemma) + "|" + word, ptype) for lemma in lemmas]]


def process_wordlist_bert(words, embs, model, filename, postype, filter_func, n_entries, graph_has_answers):
    graph = build_ruthes_graph(postype, synset_graph=True)
    lemmas = dict(graph.nodes(data="sense_lemmas"))
    answers_to_synsets = get_answers_to_synsets(postype)
    results = []
    id_name_dict = load_id_to_synset_name(postype)
    possible_answers = [x for x in list(lemmas.keys()) if lemmas[x] not in words]
    lemmas_list = [lemmas[x] for x in possible_answers]
    count = 0

    for word in words:
        vecs = load_bert_data_for_ranking(lemmas_list, embs, word, postype)
        preds = list(np.squeeze(model.predict(np.array(vecs))))
        sorted_answers = filter_func(sorted(zip(possible_answers, preds), key=lambda x: x[1], reverse=True), graph, n_entries)
        for synset, score in sorted_answers:
            results.append((word, synset, id_name_dict[synset]))
        count += 1
        print(f"processed {count} out of {len(words)}")

    if not graph_has_answers:
        with open(f"results/{filename}.tsv", "w") as io:
            for word, synset, name in results:
                io.write(f"{word}\t{synset}\t{name}\n")
    else:
        with open(f"results/{filename}.tsv", "w") as io:
            # sgraph = build_ruthes_graph(postype, synset_graph=True)
            roots = get_root_nodes(graph)

            for word, synset, name in results:
                io.write(f"{word}\t{synset}\t{name}\t{get_distance_from_synset_to_word(graph, answers_to_synsets[word], synset)}\t{get_distance_from_root_synsets(graph, synset, roots)}\n")

    with open(f"results/{filename}_nonames.tsv", "w") as io:
        for word, synset, name in results:
            io.write(f"{word}\t{synset}\n")


def get_answers_to_synsets(postype):
    graph = build_ruthes_graph(postype, synset_graph=False)
    syn_names = graph.nodes(data="synset_id")
    lemmas = graph.nodes(data="lemma")
    answers_to_synsets = {lemmas[node]: syn_names[node] for node in graph}
    return answers_to_synsets


def get_answers_to_synsets_seq(postype):
    graph = build_ruthes_graph(postype, synset_graph=False)
    syn_names = graph.nodes(data="synset_id")
    lemmas = graph.nodes(data="lemma")
    answers_to_synsets = {lemmas[node]: syn_names[node] for node in graph}
    return answers_to_synsets


def process_wordlist(words, taiga, model, filename, postype,
                     filter_func=lambda items, graph, entries: [(x[0], x[1]) for x in items[:entries]], max_entries=10, graph_has_answers=False):

    graph = build_ruthes_graph(postype, synset_graph=False)
    word_embs = [taiga.get_vector(x, postype) for x in words]
    answer_embs, answers_to_synsets = load_synsets_for_ranking(postype, taiga, graph)
    answer_embs = {x.lower(): y for x, y in answer_embs.items()}
    answers_to_synsets = {x.lower(): y for x, y in answers_to_synsets.items() if x is not None}
    possible_answers = list(answer_embs.keys())

    print(f"total words: {len(words)}, words not none: {sum([x is not None for x in word_embs])}")

    results = []
    id_name_dict = load_id_to_synset_name(postype)
    sgraph = build_ruthes_graph(postype, synset_graph=True)
    for i in range(len(words)):
        if word_embs[i] is None:
            continue
        else:
            adiffs = [taiga.get_diff(answer_embs[x], word_embs[i]) for x in possible_answers]
            preds = list(np.squeeze(model.predict(np.array(adiffs))))
            sorted_answers = sorted(zip(possible_answers, preds), key=lambda x: x[1], reverse=True)
            closest = get_synset_rankings_nomean(sorted_answers, answers_to_synsets)
            filtered_rankings = filter_func(sorted(closest.items(), key=lambda x: x[1], reverse=True), sgraph,
                                            max_entries)
            for synset, score in filtered_rankings:
                results.append((words[i], synset, id_name_dict[synset]))

        print(f"processed {i} out of {len(words)}")
    if not graph_has_answers:
        with open(f"results/{filename}.tsv", "w") as io:
            for word, synset, name in results:
                io.write(f"{word}\t{synset}\t{name}\n")
    else:
        with open(f"results/{filename}.tsv", "w") as io:
            # sgraph = build_ruthes_graph(postype, synset_graph=True)
            roots = get_root_nodes(sgraph)

            for word, synset, name in results:
                io.write(f"{word}\t{synset}\t{name}\t{get_distance_from_synset_to_word(sgraph, answers_to_synsets[word], synset)}\t{get_distance_from_root_synsets(sgraph, synset, roots)}\n")

    with open(f"results/{filename}_nonames.tsv", "w") as io:
        for word, synset, name in results:
            io.write(f"{word}\t{synset}\n")


def display_wordlist(words, taiga, graph, syngraph, model, postype, filter_func=lambda x: x, core_distance=200):
    word_embs = [taiga.get_vector(x, postype) for x in words]
    answer_embs, answers_to_synsets = load_synsets_for_ranking(postype, taiga, graph)
    answer_embs = {x.lower(): y for x, y in answer_embs.items()}
    answers_to_synsets = {x.lower(): y for x, y in answers_to_synsets.items() if x is not None}
    possible_answers = set(answer_embs.keys())

    all_figures = []

    print(f"total words: {len(words)}, words not none: {sum([x is not None for x in word_embs])}")

    results = []
    # id_name_dict = load_id_to_synset_name(postype)
    for i in range(len(words)):
        if word_embs[i] is None:
            continue
        else:
            adiffs = [taiga.get_diff(answer_embs[x], word_embs[i]) for x in possible_answers]
            preds = list(np.squeeze(model.predict(np.array(adiffs))))
            sorted_answers = sorted(zip(possible_answers, preds), key=lambda x: x[1], reverse=True)
            closest = get_synset_rankings(sorted_answers, answers_to_synsets)
            filtered_rankings = filter_func(sorted(closest.items(), key=lambda x: x[1], reverse=True))
            figs = plot_subcomponents(filtered_rankings, syngraph, core_distance)
            all_figures.append(figs)
        print(f"processed {i} out of {len(words)}")
    return all_figures


def convert_codalab_format(path, graph):
    res = []
    senses = graph.nodes(data="sense_lemmas")
    with open(path, "r", encoding="utf-8") as io:
        lines = io.readlines()
    for line in lines:
        word, synsets = line.split("\t")
        synsets = eval(synsets)
        lemmas = []
        for syn in synsets:
            lemmas.extend(senses[syn])
        for lemma in list(set(lemmas)):
            res.append((word, lemma))
    return res


def get_distance_from_synset_to_word(sgraph, child_synset, synset):
    try:
        distance = nx.shortest_path_length(sgraph, source=synset, target=child_synset)
        return distance
    except nx.NetworkXNoPath:
        return -1


def get_distance_from_root_synsets(sgraph, synset, root_synsets):
    # root_synsets = ['106509-N', '106646-N', '106613-N', '106508-N', '153471-N', '106846-N', '153782-N', '132964-N']
    distances = []
    for syn in root_synsets:
        try:
            distances.append(nx.shortest_path_length(sgraph, source=syn, target=synset))
        except nx.NetworkXNoPath:
            distances.append(10000)
    return min(distances)


def filter_func_remove_roots(items, graph, entries):
    roots = set(get_root_nodes(graph))
    return [(x[0], x[1]) for x in items if x[0] not in roots][:entries]