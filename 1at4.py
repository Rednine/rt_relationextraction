# -*- coding: utf-8 -*-
from rwn_parser import transform_training_csvs_for_ranking, POSTypes, build_ruthes_graph, load_id_to_synset_name
from embedding_ops import ModelWrapper, ModelTypes
from utils import build_zhang_gru, load_word_list, get_synset_rankings, collapse_synset_rankings
import numpy as np

model = build_zhang_gru()
model.load_weights("models/raters/zhang_gru_05012020_new_nr3.h5")
taiga = ModelWrapper(ModelTypes.W2V, "ruwikiruscorpora_upos_skipgram_300_2_2019.bin")

item_embs, answer_embs, items_to_answers, items_to_synsets, answers_to_synsets = transform_training_csvs_for_ranking("1_new", taiga, POSTypes.N)
possible_answers = list(answer_embs.keys())
possible_items = list(item_embs.keys())

words = load_word_list("nouns_public.tsv")

word_embs = [taiga.get_vector(x, POSTypes.N) for x in words]
print(f"total words: {len(words)}, words not none: {sum([x is not None for x in word_embs])}")

results = []
id_name_dict = load_id_to_synset_name(POSTypes.N)
graph = build_ruthes_graph(POSTypes.N, synset_graph=True)
for i in range(len(words)):
    if word_embs[i] is None:
        continue
    else:
        adiffs = [taiga.get_diff(answer_embs[x], word_embs[i]) for x in possible_answers]
        preds = list(np.squeeze(model.predict(np.array(adiffs))))
        sorted_answers = sorted(zip(possible_answers, preds), key=lambda x: x[1], reverse=True)
        closest = get_synset_rankings(sorted_answers, answers_to_synsets)
        filtered_rankings = collapse_synset_rankings(sorted(closest.items(), key=lambda x: x[1], reverse=True), graph, 10)
    for synset, score in filtered_rankings:
        results.append((words[i], synset, id_name_dict[synset]))
    print(f"processed {i} out of {len(words)}")

with open("results/result_N_06012020.tsv", "w") as io:
    for word, synset, name in results:
        io.write(f"{word}\t{synset}\t{name}\n")
    
