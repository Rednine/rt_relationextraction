from allennlp.data import Instance, Vocabulary, TokenIndexer, Tokenizer, Token
from allennlp.data.dataset_readers import DatasetReader
from typing import Iterator, List, Dict, Callable
from enum import Enum
from allennlp.data.token_indexers import SingleIdTokenIndexer, ELMoTokenCharactersIndexer
from allennlp.data.tokenizers import CharacterTokenizer, WordTokenizer
from allennlp.nn import RegularizerApplicator, InitializerApplicator
from allennlp.nn.util import get_text_field_mask
from allennlp.training.metrics import CategoricalAccuracy
from allennlp.data.fields import TextField, LabelField, MetadataField
from allennlp.models import Model
from allennlp.modules import Seq2VecEncoder, TextFieldEmbedder, FeedForward
import torch
from torch import nn
from typing import Optional


@DatasetReader.register("ruthes_hypernym_reader")
class RuThesReader(DatasetReader):
    def __init__(self, tokenizer=WordTokenizer(), token_indexers: Dict[str, TokenIndexer] = None):
        super().__init__(lazy=False)
        self._tokenizer = tokenizer
        self._token_indexers = token_indexers or {"elmo": ELMoTokenCharactersIndexer()}

    def _read(self, file_path: str) -> Iterator[Instance]:
        with open(file_path, encoding="utf-8") as io:
            lines = io.readlines()
        return [self.text_to_instance(line) for line in lines if line.count(",") == 4]

    def text_to_instance(self, line: str) -> Instance:
        frags = line.split(",")
        tokenized_parent = self._tokenizer.tokenize(frags[0])
        tokenized_child = self._tokenizer.tokenize(frags[1])
        distance = int(frags[2])
        parent_weight = int(frags[3])
        label = frags[4]
        parent_field = TextField(tokenized_parent, self._token_indexers)
        child_field = TextField(tokenized_child, self._token_indexers)
        label_field = LabelField(label)
        distance_field = MetadataField(distance)
        weight_field = MetadataField(parent_weight)
        return Instance({"parent": parent_field, "child": child_field, "label": label_field,
                         "distance": distance_field, "parent_weight": weight_field})


@Model.register("ruthes_hypernym_classifier")
class RuthesHypernymClassifier(Model):
    def __init__(self,
                 vocab: Vocabulary,
                 text_field_embedder: TextFieldEmbedder,
                 parent_encoder: Seq2VecEncoder,
                 child_encoder: Seq2VecEncoder,
                 feedforward: FeedForward,
                 # diff_encoder: Seq2VecEncoder,
                 # recurrent_encoder: Seq2VecEncoder,
                 initializer: InitializerApplicator = InitializerApplicator(),
                 regularizer: Optional[RegularizerApplicator] = None
                 ) -> None:
        super(RuthesHypernymClassifier, self).__init__(vocab, regularizer)
        self.embedder = text_field_embedder
        self.parent_encoder = parent_encoder
        self.child_encoder = child_encoder
        # self.diff_encoder = diff_encoder
        # self.recurrent_encoder = recurrent_encoder
        self.feedforward = feedforward
        self.initializer = initializer
        self.loss = nn.CrossEntropyLoss()
        self.accuracy = CategoricalAccuracy()

    def forward(self, parent: Dict[str, torch.Tensor], child: Dict[str, torch.Tensor], label: torch.Tensor = None,
                distance: int = 1, parent_weight: int = 1):
        embedded_w1 = self.embedder(parent)
        w1_mask = get_text_field_mask(parent)
        embedded_w2 = self.embedder(child)
        w2_mask = get_text_field_mask(child)
        encoded_w1 = self.parent_encoder(embedded_w1, w1_mask)
        encoded_w2 = self.child_encoder(embedded_w2, w2_mask)
        # print(self.parent_encoder.get_output_dim())
        diff = encoded_w1 - encoded_w2
        # diff = diff.reshape((diff.size(0), 1, diff.size(1)))
        # mask = torch.ones((diff.size(0), 1, diff.size(2)))
        # print(diff.size())
        # print(mask.size())
        # out = self.recurrent_encoder(diff, mask)
        # out, h = self.gru(encoded_diff, h)
        out = self.feedforward(diff)
        print(label.squeeze(-1))
        probs = torch.softmax(out, -1)
        output = {"logits": out, "probs": probs}
        if label is not None:
            self.accuracy(out, label.squeeze(-1))
            output["loss"] = self.loss(out, label.squeeze(-1))
        return output

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return {"accuracy": self.accuracy.get_metric(reset)}

