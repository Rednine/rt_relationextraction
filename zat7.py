from rwn_parser import POSTypes, RelTypes, build_ruthes_graph, get_graph_based_training_data, build_predict_graph
from embedding_ops import ModelWrapper, ModelTypes, evaluate_vocab
from utils import Log, create_principled_training_data, create_principled_training_data_leafcut
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
import pickle

logger = Log()
taiga = ModelWrapper(ModelTypes.W2V, "tayga_upos_skipgram_300_2_2019.bin")
ruthes = build_ruthes_graph(POSTypes.N)

data = create_principled_training_data_leafcut(ruthes, 0.002)
corpus = get_graph_based_training_data(data[0], ruthes, taiga, POSTypes.N)

#model = GradientBoostingClassifier(learning_rate=0.1, max_depth=5, verbose=1, n_estimators=200)


logger.write("GB:\n")
#model2 = LogisticRegression(verbose=1)
model2 = GradientBoostingClassifier(learning_rate=0.5, max_depth=5, verbose=1, n_estimators=400)
model2.fit(corpus[0], corpus[1])
pgraph = build_predict_graph(model2, taiga, ruthes, data, POSTypes.N)
logger.write(str(pgraph[1]))
for node, real, pred in pgraph[2]:
    logger.write(f"NODE: {node}, REAL: {repr(real)} PREDICTED: {repr(pred)} \n")
