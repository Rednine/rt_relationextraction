# -*- coding: utf-8 -*-
from enum import Enum
import matplotlib.pylab as plt

NERTypes = Enum("NERTypes", "Natasha DP DPBert")

# PREVIOUSLY IN EMBEDDING_OPS:

ModelTypes = Enum("ModelTypes", "W2V FT GLOVE ELMO BERT")

model_directory = "models/embeddings/"

# PREVIOUSLY IN RWN_PARSER:

old_storage_directory = "resources/rwn-xml-2017-05-13"
new_storage_directory = "resources/new_ruwordnet"

POSTypes = Enum("POSTypes", "A V N")
RelTypes = Enum("RelTypes", "SYN HYP ANT HPR")

type_correspondences = {
    RelTypes.SYN: "synonyms",
    RelTypes.HYP: "hyponym",
    RelTypes.ANT: "antonym",
    RelTypes.HPR: "hypernym"
}

pos_correspondences = {
    POSTypes.A: ("A", "ADJ"),
    POSTypes.N: ("N", "NOUN"),
    POSTypes.V: ("V", "VERB")
}

# PREVIOUSLY IN UTILS:

log_directory = "logs/"
graph_directory = "resources/graphs/"
#METRICS = [ 
#      keras.metrics.BinaryAccuracy(name='accuracy'),
#      keras.metrics.Precision(name='precision'),
#      keras.metrics.Recall(name='recall'),
#      keras.metrics.AUC(name='auc'),
#]

METRICS = ["accuracy"]
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

