{
    "train_data_path": "resources/training_anlp.csv",
    "validation_data_path": "resources/validation_anlp.csv",
    "dataset_reader": {
        "type": "ruthes_hypernym_reader"
    },

    "model": {
        "type": "ruthes_hypernym_classifier",
        "text_field_embedder": {
            "token_embedders": {
                "elmo": {
                    "type": "elmo_token_embedder",
                    "options_file": "models/embeddings/elmo_char_rusvectores/options.json",
                    "weight_file": "models/embeddings/elmo_char_rusvectores/model.hdf5",
                    "do_layer_norm": false,
                    "dropout": 0.3
                    }
            }
        },
        "child_encoder": {
            "type": "gru",
            "input_size": 1024,
            "hidden_size": 150,
            "bidirectional": true,
            "num_layers": 1,
            "dropout": 0.15
        },
        "parent_encoder": {
            "type": "gru",
            "input_size": 1024,
            "hidden_size": 150,
            "bidirectional": true,
            "num_layers": 1,
            "dropout": 0.15
        },
      "feedforward": {
      "input_dim": 300,
      "num_layers": 2,
      "hidden_dims": [200, 2],
      "activations": ["relu", "linear"],
      "dropout": [0.15, 0.0]
    }
    },
    "iterator": {
        "type": "bucket",
        "batch_size": 24,
        "sorting_keys": [["parent", "num_tokens"], ["child", "num_tokens"]]
    },
    "trainer": {
        "num_epochs": 300,
        "optimizer": {
            "type": "adam",
            "lr": 0.1
        },
        "patience": 10
    }
}