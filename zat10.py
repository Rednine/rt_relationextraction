# -*- coding: utf-8 -*-
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import classification_report
from embedding_ops import ModelWrapper, ModelTypes
from rwn_parser import POSTypes, load_training_csvs
import wandb
from pickle import dumps
from os import path


taiga = ModelWrapper(ModelTypes.W2V, "ruwikiruscorpora_upos_skipgram_300_2_2019.bin")

if __name__ == '__main__':

    wandb.init(project="relationextraction")
    config = wandb.config
    config.lr = 0.1
    config.max_depth = 5
    config.max_features = "sqrt"
    config.n_estimators = 350
    config.min_samples_split = 0.1
    
    X_tr, X_val, y_tr, y_val = load_training_csvs("0", taiga, POSTypes.N)
    
    model = GradientBoostingClassifier(learning_rate=config.lr, max_depth=config.max_depth, max_features=config.max_features, verbose=2, n_estimators=config.n_estimators, min_samples_split=config.min_samples_split)
    model.fit(X_tr, y_tr)
    print("Model fitted.")
    preds = model.predict(X_val)
    classreport = classification_report(y_val, preds)
    wandb.log(classreport)
    wandb.log({"Train Accuracy": model.score(X_tr, y_tr), "Test Accuracy": model.score(X_val, y_val)})
    from pickle import dump
    with open("models/raters/gb_glad_violed_27.pkl", "wb")as io: dump(model, io)
    print("Done.")
