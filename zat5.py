from rwn_parser import *
from embedding_ops import *
from utils import *
from sklearn.ensemble import GradientBoostingClassifier

# the purpose of this zero approximation test is to get a graph-based metric for the model from zat1
if __name__ == '__main__':

    log = Log()
    graph = build_ruthes_graph(POSTypes.N)
    data = create_principled_training_data(graph, 0.3, 55)
    embs = ModelWrapper(ModelTypes.W2V, "model.bin")
    train = get_graph_based_training_data(data[0], graph, embs, POSTypes.N)
    model = GradientBoostingClassifier(learning_rate=0.1, max_depth=5)
    model.fit(train)