from datetime import datetime
from enum import Enum
from time import time
from typing import List, Dict, Tuple, Any, Union
from random import shuffle

from tensorflow.keras import Model
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import precision_recall_fscore_support, classification_report
from numpy import mean, array, random
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Attention
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier
from tensorflow.keras.layers import Dropout, Dense, Input, GRU, Activation, InputLayer, Reshape, Conv1D, GlobalMaxPooling1D, \
    GlobalAveragePooling1D, Concatenate
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint, LearningRateScheduler
import networkx as nx
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
import bokeh
from pickle import load, dump
from bokeh.plotting import figure, from_networkx
from wandb.keras import WandbCallback
import wandb
from networkx.drawing.nx_pydot import graphviz_layout
from constants import log_directory, graph_directory, METRICS, colors
import numpy as np
import pandas as pd

from deeppavlov.core.common.registry import register
from deeppavlov.core.data.dataset_reader import DatasetReader
from deeppavlov.models.preprocessors.bert_preprocessor import BertPreprocessor
from deeppavlov.models.preprocessors.transformers_preprocessor import TransformersBertPreprocessor, _pad
from bert_dp.preprocessing import convert_examples_to_features, InputExample, InputFeatures


class Log:
    def __init__(self):
        self.log = open(log_directory + str(datetime.now().timestamp()) + ".log", "a", encoding="utf8")

    def write(self, string):
        self.log.write("\n")
        self.log.write(datetime.now().isoformat())
        self.log.write("\n")
        self.log.write(string)
        self.log.flush()


def transform_data(pos_data: list, neg_data: list, ptype: Enum, model, logger: Log) -> tuple:
    features = []
    labels = []
    skipped = 0
    list_of_skipped = []
    for d in pos_data:
        diff = model.get_diff_from_words(d[0], d[1], ptype)
        if diff is None:
            skipped += 1
            list_of_skipped.append((d[0], d[1]))
            continue
        features.append(diff)
        labels.append(1)
    for d in neg_data:
        diff = model.get_diff_from_words(d[0], d[1], ptype)
        if diff is None:
            skipped += 1
            list_of_skipped.append((d[0], d[1]))
            continue
        features.append(diff)
        labels.append(0)
    logger.write(f"TOTAL OOV ITEMS: {skipped}")
    # for item in list_of_skipped:
        # logger.write(repr(item))
    return features, labels


def run_test_with_kfold(data, classifier, logger, nfolds):
    skf = StratifiedKFold(n_splits=nfolds, shuffle=True)
    precs = []
    recs = []
    fs = []
    for train_i, test_i in skf.split(data[0], data[1]):
        classifier.fit(array(data[0])[train_i], array(data[1])[train_i])
        pred = classifier.predict(array(data[0])[test_i])
        scores = precision_recall_fscore_support(array(data[1])[test_i], pred, average="binary")
        precs.append(scores[0])
        recs.append(scores[1])
        fs.append(scores[2])
        logger.write(f"Completed fit for synonyms and GB, scores: {repr(scores)}")
    logger.write(f"Average scores for synonyms and GB - prec: {mean(precs)} rec: {mean(recs)} f1: {mean(fs)}")

    return precs, recs, fs


def build_classifier_nn():
    model = Sequential()
    model.add(Input(shape=(None, 300)))
    model.add(Dense(100, kernel_initializer="normal", activation="relu"))
    model.add(Dense(50, kernel_initializer="normal", activation="relu"))
    model.add(Dense(100, kernel_initializer="normal", activation="relu"))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])
    return model


def get_classifier_nn():
    return KerasClassifier(build_fn=build_classifier_nn, epochs=25, batch_size=25)


def build_zhang_gru():
    model = Sequential()
    model.add(InputLayer((300,)))
    model.add(Reshape((1, 300,)))
    model.add(GRU(200, return_sequences=False))
    model.add(Activation('relu'))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model


def build_zhang_gru_for_elmo():
    model = Sequential()
    model.add(InputLayer((2048,)))
    model.add(Reshape((1, 2048,)))
    model.add(GRU(200, return_sequences=False))
    model.add(Activation('relu'))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model

def build_zhang_gru_for_elmo_short():
    model = Sequential()
    model.add(InputLayer((1024,)))
    model.add(Reshape((1, 1024,)))
    model.add(GRU(200, return_sequences=False))
    model.add(Activation('relu'))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model

def build_zhang_gru_for_elmo_short2():
    model = Sequential()
    model.add(InputLayer((1024,)))
    model.add(Reshape((1, 1024,)))
    model.add(Dropout(0.15))
    model.add(GRU(400, return_sequences=True))
    model.add(Activation('relu'))
    model.add(Dropout(0.15))
    model.add(GRU(400, return_sequences=False))
    model.add(Activation('relu'))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model


def build_zhang_gru_for_elmo_short3():
    model = Sequential()
    model.add(InputLayer((1024,)))
    model.add(Reshape((1, 1024,)))
    model.add(Dropout(0.15))
    model.add(GRU(400, return_sequences=True))
    model.add(Activation('relu'))
    model.add(Dropout(0.15))
    model.add(GRU(400, return_sequences=False))
    model.add(Activation('relu'))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="hinge", optimizer="adam", metrics=METRICS)
    return model


def build_zhang_gru_for_elmo_long2():
    model = Sequential()
    model.add(InputLayer((2048,)))
    model.add(Reshape((1, 2048,)))
    model.add(Dropout(0.15))
    model.add(GRU(400, return_sequences=True))
    model.add(Activation('relu'))
    model.add(Dropout(0.15))
    model.add(GRU(400, return_sequences=False))
    model.add(Activation('relu'))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model


def build_sequence_gru_for_elmo():
    model = Sequential()
    model.add(InputLayer((22, 1024)))
    model.add(GRU(400, return_sequences=False, activation="relu"))
    model.add(Dropout(0.15))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model


def build_sequence_cnn_for_elmo():
    model = Sequential()
    model.add(InputLayer((22, 1024)))
    model.add(Dropout(0.1))
    model.add(Conv1D(filters=300, kernel_size=3, padding="valid", strides=1, activation="relu"))
    model.add(GlobalMaxPooling1D())
    model.add(Dense(300, kernel_initializer="normal", activation="relu"))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model


def build_cnn_with_attention_for_elmo():
    input_parent = Input(shape=(11, 1024))
    input_child = Input(shape=(11, 1024))

    cnn_layer = Conv1D(
        filters=300,
        kernel_size=4,
        # Use 'same' padding so outputs have the same shape as inputs.
        padding='same', activation="relu")

    cnn_layer2 = Conv1D(
        filters=300,
        kernel_size=4,
        # Use 'same' padding so outputs have the same shape as inputs.
        padding='same', activation="relu")
    concat_input = Concatenate()([input_parent, input_child])
    cnn_encoding = cnn_layer(concat_input)
    # Value encoding of shape [batch_size, Tv, filters].
    # value_seq_encoding = cnn_layer(input_child)
    query_value_attention_seq = Attention()(
        [cnn_encoding, cnn_encoding])
    # query_encoding = GlobalAveragePooling1D()(
    #     query_seq_encoding)


    second_encoding = cnn_layer2(query_value_attention_seq)
    second_attention = Attention()([second_encoding, second_encoding])
    input_layer = GlobalAveragePooling1D()(second_attention)
    #value_encoding = GlobalAveragePooling1D()(value_seq_encoding) # not here in the original
    # input_layer = Concatenate()(
    #     [query_encoding, value_encoding, query_value_attention])
    deep_layer_1 = Dense(200, kernel_initializer="normal", activation="relu")
    deep_layer_2 = Dense(1, kernel_initializer="normal", activation="sigmoid")
    dl1_out = deep_layer_1(input_layer)
    dl2_out = deep_layer_2(dl1_out)
    model = Model(inputs=[input_parent, input_child], outputs=dl2_out)
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model


def build_gru_with_attention_for_elmo():
    input_parent = Input(shape=(11, 1024))
    input_child = Input(shape=(11, 1024))
    conc_input = Concatenate(axis=1)([input_parent, input_child])
    gru_layer = GRU(400, return_sequences=True, activation="relu")
    gru_layer2 = GRU(400, return_sequences=True, activation="relu")

    query_seq_encoding = gru_layer(conc_input)
    # Value encoding of shape [batch_size, Tv, filters].
    #value_seq_encoding = gru_layer(input_child)
    query_value_attention_seq = Attention()(
        [query_seq_encoding, query_seq_encoding])
    query_seq_encoding2 = gru_layer2(query_value_attention_seq)
    query_value_attention_seq2 = Attention()([query_seq_encoding2, query_seq_encoding2])
    query_encoding = GlobalAveragePooling1D()(
        query_value_attention_seq2)
    # value_encoding = GlobalAveragePooling1D()(value_seq_encoding)
    # query_value_attention = GlobalAveragePooling1D()(
    #     query_value_attention_seq)
    # input_layer = Concatenate()(
    #     [query_encoding, value_encoding, query_value_attention])
    # deep_layer_1 = Dense(200, kernel_initializer="normal", activation="relu")
    deep_layer_2 = Dense(1, kernel_initializer="normal", activation="sigmoid")
    # dl1_out = deep_layer_1(input_layer)
    dl2_out = deep_layer_2(query_encoding)
    model = Model(inputs=[input_parent, input_child], outputs=dl2_out)
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model


def build_zhang_gru_for_bert():
    model = Sequential()
    model.add(InputLayer((768,)))
    model.add(Reshape((1, 768,)))
    model.add(Dropout(0.15))
    model.add(GRU(400, return_sequences=True))
    model.add(Activation('relu'))
    model.add(Dropout(0.15))
    model.add(GRU(400, return_sequences=False))
    model.add(Activation('relu'))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model


def get_synset_rankings(preds, answers_to_synsets, cutoff=None):
    scores = {}
    for pred, val in preds:
        pred_synset = answers_to_synsets[pred]
        if not scores.get(pred_synset):
            scores[pred_synset] = [val, 1]
        else:
            scores[pred_synset] = [scores[pred_synset][0] + val, scores[pred_synset][1] + 1]
    
    return {syn: val[0] / val[1] for syn, val in scores.items() if not cutoff or val[1] < cutoff}


def get_synset_rankings_dummy(preds, answers_to_synsets, cutoff=None):
    return {answers_to_synsets[pred]: val for pred, val in preds}


def get_synset_rankings_nomean(preds, answers_to_synsets):
    scores = {}
    for pred, val in preds:
        pred_synset = answers_to_synsets[pred]
        if not scores.get(pred_synset) or scores[pred_synset] < val:
            scores[pred_synset] = val
        
    return scores


def collapse_synset_rankings(rankings, graph, core_distance):
    new_rankings = rankings
    while True:
        indices_for_removal = []
        for i in range(core_distance):
            for j in range(core_distance):
                if graph.has_edge(rankings[i][0], rankings[j][0]) and rankings[j][1] >= (rankings[i][1] - 0.2):
                    indices_for_removal.append(i)
        
        if not indices_for_removal:
            break
        for i in indices_for_removal:
            new_rankings.pop(i)
        
    return new_rankings[:core_distance]
    

def plot_subcomponents(rankings, graph, core_distance):
    figs = []
    subcomp = graph.subgraph([x[0] for x in rankings][:core_distance])
    print([x[0] for x in rankings][:10])
    #print(graph.nodes[:10])
    print(len(subcomp))
    for comp in nx.weakly_connected_components(subcomp):
        if len(comp) < 3:
            continue
        print(type(comp))
        print(comp)
        gr = subcomp.subgraph(comp)
        fig = figure()
        layout = from_networkx(gr, graphviz_layout(gr, prog="dot"))
        fig.renderers.append(layout)
        bokeh.io.show(fig)
        figs.append(fig)
    return figs
        
        

def load_word_list(path):
    with open("resources/" + path) as io:
        return [x.strip() for x in io.readlines()]           
    

def build_zhang2_gru():
    model = Sequential()
    model.add(InputLayer((300,)))
    model.add(Reshape((1, 300,)))
    model.add(GRU(200, return_sequences=True))
    model.add(Activation('relu'))
    model.add(GRU(200, return_sequences=False))
    model.add(Activation('relu'))
    model.add(Dense(1, kernel_initializer="normal", activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=METRICS)
    return model


def plot_metrics(history):
  metrics =  ['loss', 'auc', 'precision', 'recall']
  for n, metric in enumerate(metrics):
    name = metric.replace("_"," ").capitalize()
    plt.subplot(2,2,n+1)
    plt.plot(history.epoch,  history.history[metric], color=colors[0], label='Train')
    plt.plot(history.epoch, history.history['val_'+metric],
             color=colors[0], linestyle="--", label='Val')
    plt.xlabel('Epoch')
    plt.ylabel(name)
    if metric == 'loss':
      plt.ylim([0, plt.ylim()[1]])
    elif metric == 'auc':
      plt.ylim([0.8,1])
    else:
      plt.ylim([0,1])

    plt.legend()


def get_zhang_gru():
    return KerasClassifier(build_fn=build_zhang_gru, epochs=25, batch_size=25)


def create_principled_training_data(graph: nx.DiGraph, split: float, seed: int):
    roots = get_root_nodes(graph)
    training_nodes = set()
    test_nodes = set()
    add_to_training = True
    complete = False
    for node in graph.nodes:
        training_nodes.add(node)
    while (float(len(test_nodes)) / len(graph)) < split:
        for root in roots:
            dfs = nx.dfs_preorder_nodes(graph, root)
            for node in dfs:
                if add_to_training:
                    flip = random.uniform(0, 1)
                    if flip > 0.6:
                        add_to_training = False
                elif not add_to_training and (node not in test_nodes):
                    test_nodes.add(node)
                    if node in training_nodes:
                        training_nodes.remove(node)
                    print(f"Num test nodes: {len(test_nodes)}, num train nodes: {len(training_nodes)}, "
                          f"ratio: {float(len(test_nodes))/ len(graph)}")
                assert len(graph) == len(training_nodes) + len(test_nodes)
                if not nx.descendants(graph, node):
                    add_to_training = True
                if (float(len(test_nodes)) / len(graph)) >= split:
                    complete = True
            if complete:
                break

    return training_nodes, test_nodes

    print("Done")


def create_principled_training_data_leafcut(graph:nx.DiGraph, split:float):
    training_nodes = set(graph.nodes)
    test_nodes = set()

    for leaf in get_leaf_node(graph):
        if len(test_nodes) / float(len(training_nodes) + len(test_nodes)) < split:
            training_nodes.remove(leaf)
            test_nodes.add(leaf)
            print(f"Acquired {len(test_nodes)} out of {split * float(len(graph))}")
        else:
            break

    return training_nodes, test_nodes


def get_leaf_node(graph:nx.DiGraph):
    already_cut = set()
    # leaves = set()
    leaves = set([x for x in graph.nodes if len(list(graph.successors(x))) == 0])
    nodeset = set(graph.nodes)
    nodeset.difference_update(leaves)
    assert len(graph) == len(nodeset) + len(leaves)
    while len(already_cut) != len(graph):
        ret = random.choice(list(leaves))
        leaves.remove(ret)
        already_cut.add(ret)
        candidates = {x for x in graph.predecessors(ret) if (x in nodeset) and all([y in already_cut for y in graph.successors(x)])}
        leaves.update(candidates)
        nodeset.difference_update(candidates)
        yield ret


def draw_digraph(G: nx.DiGraph):
    pos = nx.layout.spring_layout(G)

    nx.draw(G, pos, node_size=1500, edge_cmap=plt.cm.get_cmap("Purples"))
    pylab.show()


def get_root_nodes(G: nx.DiGraph):
    return [node for node in G.nodes if G.in_degree(node) == 0]


def hits_at_k(graph:nx.DiGraph, new_graph:nx.DiGraph, nodes_in_train):
    """
    THIS IS DEPRECATED
    """
    rates = []

    for node in nodes_in_train:
        actual_children = set(graph.successors(node))
        predicted_children = set(new_graph.successors(node))
        correctly_predicted = predicted_children.intersection(actual_children)
        try:
            score = len(correctly_predicted) / float(len(actual_children))
            rates.append(score)
        except ZeroDivisionError:
            continue

    return mean(rates)


def hak(node, preds, graph:nx.DiGraph, distance=None):
    actual_parents = list(graph.predecessors(node))
    if distance:
        actual_parents = [x for x in actual_parents if nx.shortest_path_length(graph, x, node) <= distance]
    
    count = sum([pred in actual_parents for pred in preds])
    
    return float(count) / len(preds)


def mean_hak(nodes, preds, graph, distance):
    return float(sum([hak(node, preds, graph, distance) for node in nodes])) / len(nodes)


def get_prediction_differences(graph:nx.DiGraph, new_graph:nx.DiGraph, nodes_in_test):
    pairs = []
    for node in nodes_in_test:
        actual_children = set(graph.predecessors(node))
        try:

            predicted_children = set(new_graph.predecessors(node))
            pairs.append((node, actual_children, predicted_children))
        except nx.NetworkXError:
            pairs.append((node, actual_children, {"NODE NOT ATTACHED"}))
    return pairs


def draw_graph(graph):
    pass


def save_graph(graph:nx.DiGraph):

    nx.write(graph, graph_directory + str(datetime.now().timestamp()) + ".gml")
    
    
def reciprocal_rank(results:list, correct:str):
    try:
        ix = results.index(correct)
    except ValueError or ix > 10:
        ix = 10
    return 1.0 / (ix + 1)


def mean_reciprocal_rank(results, answers):
    rank = 0
    for i in range(len(results)):
        rank += reciprocal_rank(results[i], answers[i])
    return float(rank) / len(results)

            
def run_classifier_tests(build_fn, X_tr, X_te, y_tr, y_te, epochs, pos_w, batch_size, savefile):
    weights = {0: 1.0, 1:pos_w}
    classifier = build_fn()
    classifier.fit(np.array(X_tr), y_tr, batch_size=batch_size, epochs=epochs, validation_data=(np.array(X_te), y_te), class_weight=weights)
    classifier.save_weights(f"models/raters/{savefile}.hdf5")
    preds = classifier.predict(np.array(X_te))
    print(f"Done testing model {savefile}")
    print(classification_report(y_te, [int(round(x)) for x in np.squeeze(preds)]))
    print(better_confusion_matrix(y_te, [int(round(x)) for x in np.squeeze(preds)]))
    
    return classifier


def run_classifier_tests_generator(build_fn, train_gen, test_gen, epochs, pos_w, batch_size, max_seq_len, savefile, dual=False):
    weights = {0: 1.0, 1: pos_w}
    classifier = build_fn()
    reduce_lr = ReduceLROnPlateau(factor=0.3, patience=1)
    #early_stop = EarlyStopping(patience=2, verbose=1, restore_best_weights=True)
    checkpoint = ModelCheckpoint(filepath=f"models/raters/{savefile}-checkpoint.hdf5", save_best_only=True, save_weights_only=True)
    classifier.fit_generator(train_gen.generate(max_seq_len, batch_size, dual),
                             validation_data=test_gen.generate(max_seq_len, batch_size, dual),
                             steps_per_epoch=len(train_gen.lines)/batch_size, epochs=epochs, class_weight=weights,
                             validation_steps=2500, callbacks=[reduce_lr, checkpoint])
    classifier.save_weights(f"models/raters/{savefile}.hdf5")
    X_te, y_te = test_gen.get_sample(1000, max_seq_len, dual_inputs=dual)
    if dual:
        preds = classifier.predict(X_te)
    else:
        preds = classifier.predict(np.array(X_te))
    print(classification_report(y_te, [int(round(x)) for x in np.squeeze(preds)]))

    return classifier


def train_full_classifier_generator(build_fn, train_gen, test_gen, epochs, pos_w, batch_size, max_seq_len, savefile, dual=False):
    weights = {0: 1.0, 1: pos_w}
    classifier = build_fn()
    reduce_lr = ReduceLROnPlateau(factor=0.3, patience=1)
    #early_stop = EarlyStopping(patience=2, verbose=1, restore_best_weights=True)
    checkpoint = ModelCheckpoint(filepath=f"models/raters/{savefile}-checkpoint.hdf5", save_best_only=True, save_weights_only=True)
    classifier.fit_generator(train_gen.generate(max_seq_len, batch_size, dual),
                             validation_data=test_gen.generate(max_seq_len, batch_size, dual),
                             steps_per_epoch=len(train_gen.lines)/batch_size, epochs=epochs, class_weight=weights,
                             validation_steps=2500, callbacks=[reduce_lr, checkpoint])

    classifier.save_weights(f"models/raters/{savefile}.hdf5")
    X_te, y_te = test_gen.get_sample(1000, max_seq_len, dual_inputs=dual)
    preds = classifier.predict(X_te)
    print(classification_report(y_te, [int(round(x)) for x in np.squeeze(preds)]))

    return classifier


def resume_training_generator(model, train_gen, test_gen, epochs, pos_w, batch_size, max_seq_len, savefile, lr):
    weights = {0: 1.0, 1: pos_w}
    lr_scheduler = LearningRateScheduler(lambda epoch, ler: lr)
    reduce_lr = ReduceLROnPlateau(factor=0.2, patience=1)
    #early_stop = EarlyStopping(patience=2, verbose=1, restore_best_weights=True)
    checkpoint = ModelCheckpoint(filepath=f"models/raters/{savefile}-checkpoint.hdf5", save_best_only=True,
                                 save_weights_only=True)
    model.fit_generator(train_gen.generate(max_seq_len, batch_size, dual_inputs=True),
                             validation_data=test_gen.generate(max_seq_len, batch_size, dual_inputs=True),
                             steps_per_epoch=len(train_gen.lines) / batch_size, epochs=epochs, class_weight=weights,
                             validation_steps=3500, callbacks=[reduce_lr, lr_scheduler, checkpoint])
    model.save_weights(f"models/raters/{savefile}.hdf5")
    X_te, y_te = test_gen.get_sample(1000, max_seq_len, dual_inputs=True)
    preds = model.predict(X_te)
    print(classification_report(y_te, [int(round(x)) for x in np.squeeze(preds)]))
    print(better_confusion_matrix(y_te, [int(round(x)) for x in np.squeeze(preds)]))

    return model


def run_classifier_tests_trained(model, test_gen, max_seq_len, dual=False):
    X_te, y_te = test_gen.get_sample(7000, max_seq_len, dual_inputs=dual)
    if not dual:
        preds = model.predict(np.array(X_te))
    else:
        preds = model.predict(X_te)
    print(classification_report(y_te, [int(round(x)) for x in np.squeeze(preds)]))
    print(better_confusion_matrix(y_te, [int(round(x)) for x in np.squeeze(preds)]))


def train_full_classifier(build_fn, X_tr, X_te, y_tr, y_te, epochs, pos_w, batch_size, savefile):
    weights = {0: 1.0, 1:pos_w}
    classifier = build_fn()
    
    classifier.fit(np.array(X_tr + X_te), np.array(list(y_tr) + y_te), batch_size=batch_size, epochs=epochs, validation_data=(np.array(X_te), y_te), class_weight=weights)
    classifier.save_weights(f"models/raters/{savefile}.hdf5")
    preds = classifier.predict(np.array(X_te))
    print(classification_report(y_te, [int(round(x)) for x in np.squeeze(preds)]))
    
    return classifier


def better_confusion_matrix(y_true, y_pred):
    return pd.crosstab(pd.Series(y_true), pd.Series(y_pred), rownames=['True'], colnames=['Predicted']).apply(lambda r: 100.0 * r / r.sum())


def get_hak_list_from_table(path, crit_values_list, len_l=1000):
    table = pd.read_csv(path, sep="\t", header=None, index_col=None)
    j = 0
    ret = []
    temp_vals = []
    vals = []
    for cv in crit_values_list:
        while j != len_l:
            for i in range(10):
                val = table.loc[j + i][5]
                temp_vals.append(int((val <= cv) and (val != -1)))
            j += 10
            vals.append(sum(temp_vals))
            temp_vals = []
        ret.append(np.mean(vals))
        vals = []
        j = 0
    return ret


def load_bert_training_files(suffix):
    res = []
    for i in range(4):
        path = f"resources/{i}{suffix}.pkl"
        with open(path, "rb") as io:
            res.append(load(io))

    xtr, xte, ytr, yte = res
    return xtr, xte, ytr, yte


def save_training_files(xtr, xte, ytr, yte, suffix):
    for i, arr in enumerate([xtr, xte, ytr, yte]):
        path = f"resources/{i}{suffix}.pkl"
        with open(path, "wb") as io:
            dump(arr, io)


def get_mrr_from_table(path, crit_values_list, len_l=1000):
    table = pd.read_csv(path, sep="\t", header=None, index_col=None)
    j = 0
    ret = []
    temp_vals = 0.1
    vals = []
    for cv in crit_values_list:
        while j != len_l:
            for i in range(10):
                val = table.loc[j + i][5]
                if (val <= cv) and (val != -1):
                    temp_vals = 1.0 / (i + 1)
                    break
            j += 10
            vals.append(temp_vals)
            temp_vals = 0.1
        ret.append(np.mean(vals))
        vals = []
        j = 0
    return ret


def get_map_from_table(path, crit_values_list):
    pass


def load_taiga_corpus(path):
    pass


def mark_ner(path):
    pass


def train_elmo(path):
    pass


@register("rt_relex_dataset_reader")
class RtRelexDatasetReader(DatasetReader):
    def read(self, data_path: str, *args, **kwargs) -> Dict[str, List[Tuple[Any, Any]]]:
        print("Started loading training CSV")
        ctime = time()
        X_train, X_val, y_train, y_val = [], [], [], []
        with open(f"{data_path}/bert_training_{kwargs['suffix']}.csv", encoding="utf-8") as io:
            count = 0
            lines = io.readlines()
            for line in lines:
                frags = line.strip().split("\t")

                parent = frags[2]
                child = frags[3]
                label = str(int(int(frags[-1]) != 0))
                diff = ", ".join(eval(parent)) + "|" + child
                count += 1
                if diff is not None:
                    X_train.append(diff)
                    y_train.append(label)
        with open(f"{data_path}/bert_validation_{kwargs['suffix']}.csv", encoding="utf-8") as io:
            count = 0
            lines = io.readlines()
            for line in lines:
                frags = line.strip().split("\t")
                parent = frags[2]
                child = frags[3]
                label = str(int(int(frags[-1]) != 0))
                diff = ", ".join(eval(parent)) + "|" + child
                count += 1

                if diff is not None:
                    X_val.append(diff)
                    y_val.append(label)

        ftime = time()
        print(f"Loaded, time elapsed: {ftime - ctime}")
        test = list(zip(X_val, y_val))
        print(f"Train length {len(X_train)}, test length {int(len(test) * (1 - kwargs['val_prop']))}, val length {int(len(test) * kwargs['val_prop'])}")
        shuffle(test)
        return {"train": list(zip(X_train, y_train)), "test": test[:int(len(test) * (1 - kwargs["val_prop"]))], "valid": test[int(len(test) * kwargs["val_prop"]):]}


@register('bert_relex_preprocessor')
class BertRelexPreprocessor(BertPreprocessor):
    current_converted_samples = 0

    def __call__(self, texts_a: List[str]) -> List[InputFeatures]:
        """Call Bert :func:`bert_dp.preprocessing.convert_examples_to_features` function to tokenize and create masks.

        texts_a and texts_b are separated by [SEP] token

        Args:
            texts_a: list of texts, with | separations between parts

        Returns:
            batch of :class:`bert_dp.preprocessing.InputFeatures` with subtokens, subtoken ids, subtoken mask, segment mask.

        """


        # unique_id is not used
        examples = [InputExample(unique_id=0, text_a=text_a, text_b=text_b)
                    for text_a, text_b in [x.split("|") for x in texts_a]]
        self.current_converted_samples += 1
        print(f"Converting sample {self.current_converted_samples}, batch size {len(texts_a)}")
        return convert_examples_to_features(examples, self.max_seq_length, self.tokenizer)


@register('relex_embedder_preprocessor')
class RelexEmbedderPreprocessor(TransformersBertPreprocessor):
    def __call__(self, tokens_batch: Union[List[str], List[List[str]]]) -> \
            Tuple[List[List[str]], List[List[str]], np.ndarray, np.ndarray, np.ndarray]:

        if isinstance(tokens_batch[0], str):  # skip for already tokenized text
            tokens_batch = [self.tokenizer.basic_tokenizer.tokenize(sentence, self.tokenizer.all_special_tokens)
                            for sentence in tokens_batch]
        startofword_markers_batch = []
        subtokens_batch = []
        for tokens in tokens_batch:
            startofword_markers = [0]
            subtokens = ['[CLS]']
            for token in tokens:
                for i, subtoken in enumerate(self.tokenizer.wordpiece_tokenizer.tokenize(token)):
                    startofword_markers.append(int(i == 0))
                    if subtoken != "|":
                        subtokens.append(subtoken)
                    else:
                        subtokens.append('[SEP]')
            startofword_markers.append(0)
            subtokens.append('[SEP]')
            if len(subtokens) > self.max_seq_length:
                raise RuntimeError(f"input sequence after bert tokenization"
                                   f" cannot exceed {self.max_seq_length} tokens.")

            startofword_markers_batch.append(startofword_markers)
            subtokens_batch.append(subtokens)

        encoded = self.tokenizer.batch_encode_plus([[subtokens, None] for subtokens in subtokens_batch],
                                                   add_special_tokens=False)

        return (tokens_batch, subtokens_batch,
                _pad(encoded['input_ids'], value=self.tokenizer.pad_token_id),
                _pad(startofword_markers_batch), _pad(encoded['attention_mask']))


def rank_row(row, lts, graph):
    nodes_in_row = lts[row[0]]
    answer = row[1]
    shortest_path = -1
    for node in nodes_in_row:
        try:
            path = nx.shortest_path_length(graph, answer, node)
            if shortest_path == -1 or path < shortest_path:
                shortest_path = path
        except nx.NetworkXNoPath:
            continue
    return shortest_path


def evaluate_ranking_list(name, syngraph, new_name):
    csv = pd.read_csv(f"results/{name}.tsv", sep="\t", header=None)
    synsets_to_lemmas = dict(syngraph.nodes(data="sense_lemmas"))
    lemmas_to_synsets = {}
    for key, value in synsets_to_lemmas.items():
        for lemma in value:
            if not lemmas_to_synsets.get(lemma):
                lemmas_to_synsets[lemma] = []
            lemmas_to_synsets[lemma].append(key)

    csv[5] = csv.apply(lambda row: rank_row(row, lemmas_to_synsets, syngraph), axis="columns")
    csv.to_csv(f"results/{new_name}.tsv", sep="\t", header=False, index=False)

