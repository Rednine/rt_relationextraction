from sklearn.ensemble import GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV, train_test_split, KFold
# from multiprocessing.dummy import Pool
from rwn_parser import *
from embedding_ops import *
from utils import *

# the purpose of this zeroth approximation test is to just throw a few models seen in the literature at lexrels vs. random words
# performance is expected to be reasonably high
if __name__ == '__main__':

    log = Log()
    embedding_model = ModelWrapper(ModelTypes.W2V, "model.bin")

    syns = get_relations(RelTypes.SYN, "N", True)
    # ants = get_relations(RelTypes.ANT, "N", True)
    hyps = get_relations(RelTypes.HYP, "N", True)
    hprs = get_relations(RelTypes.HPR, "N", True)
    hyps.extend(hprs)
    neg_syns = get_negative_examples(len(syns), RelTypes.SYN, "N", True)
    # neg_ants = get_negative_examples(10000, RelTypes.ANT, "N", True)
    neg_hyps = get_negative_examples(int(len(hyps) / 2), RelTypes.HPR, "N", True)
    neg_hyps.extend(get_negative_examples(int(len(hyps) / 2), RelTypes.HYP, "N", True))

    hyp_data = transform_data(hyps, neg_hyps, POSTypes.N, embedding_model, log)
    syn_data = transform_data(syns, neg_syns, POSTypes.N, embedding_model, log)

    hyp_corp = train_test_split(hyp_data)
    syn_corp = train_test_split(syn_data)

    print("Fitting SGD1...")
    model_svc = SGDClassifier(verbose=1)
    paramgrid_svc = {"alpha": [0.0001, 0.001]}
    cv_svc = GridSearchCV(model_svc, paramgrid_svc, n_jobs=4)
    cv_svc.fit(syn_data[0], syn_data[1])
    log.write("SVC, synonyms:")
    log.write(repr(cv_svc.best_params_))
    log.write(repr(cv_svc.best_score_))
    log.write(repr(cv_svc.cv_results_))

    print("Fitting SGD2...")
    model_svc = SGDClassifier(verbose=1)
    cv_svc2 = GridSearchCV(model_svc, paramgrid_svc, n_jobs=4)
    cv_svc2.fit(hyp_data[0], hyp_data[1])
    log.write("SVC, hypernyms:")
    log.write(repr(cv_svc2.best_params_))
    log.write(repr(cv_svc2.best_score_))
    log.write(repr(cv_svc2.cv_results_))

    print("Fitting GB1...")
    model_GB = GradientBoostingClassifier(verbose=1)
    paramgrid_GB = {"learning_rate": [0.1, 0.9], "max_depth": [None, 5]}
    cv_GB = GridSearchCV(model_GB, paramgrid_GB, n_jobs=4)
    cv_GB.fit(syn_data[0], syn_data[1])
    log.write("GB, synonyms:")
    log.write(repr(cv_GB.best_params_))
    log.write(repr(cv_GB.best_score_))
    log.write(repr(cv_GB.cv_results_))

    print("Fitting GB2...")
    model_GB2 = GradientBoostingClassifier(verbose=1)
    cv_GB2 = GridSearchCV(model_GB, paramgrid_GB, n_jobs=4)
    cv_GB2.fit(hyp_data[0], hyp_data[1])
    log.write("GB, hypernyms:")
    log.write(repr(cv_GB2.best_params_))
    log.write(repr(cv_GB2.best_score_))
    log.write(repr(cv_GB2.cv_results_))
