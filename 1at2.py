# -*- coding: utf-8 -*-
from rwn_parser import load_training_csvs, POSTypes
from embedding_ops import ModelWrapper, ModelTypes
from utils import build_zhang_gru, build_zhang2_gru
from sklearn.metrics import classification_report
import numpy as np
from keras.callbacks import EarlyStopping

# training the GRU model from (Zhang whenever)

#wandb.init()

weights = {0: 1.0, 1:4.0}
taiga = ModelWrapper(ModelTypes.W2V, "ruwikiruscorpora_upos_skipgram_300_2_2019.bin")
X_tr, X_te, y_tr, y_te = load_training_csvs("1_new_nr3", taiga, POSTypes.N)
y_tr = np.array(y_tr)
classifier = build_zhang2_gru()
classifier.fit(np.array(X_tr), y_tr, batch_size=1000, epochs=100, validation_data=(np.array(X_te), y_te), class_weight=weights, callbacks=[EarlyStopping(patience=5, restore_best_weights=True)])
classifier.save("models/raters/zhang_gru_05012020_new_nr3_2l.h5")
preds = classifier.predict(np.array(X_te))
print(classification_report(y_te, [int(round(x)) for x in np.squeeze(preds)]))
