from allennlp_utils import RuThesReader, RuthesHypernymClassifier

train_path = "resources/training_anlp.csv"
validation_path = "resources/validation_anlp.csv"
elmo_options_path = "models/embeddings/elmo_char_rusvectores/options.json"
elmo_weights_path = "models/embeddings/elmo_char_rusvectores/model.hdf5"
training_folder = "models/raters/exp1_elmo_gru/"

reader = RuThesReader()
