from embedding_ops import ModelWrapper, ModelTypes
from rwn_parser import load_id_to_synset_name
from utils import get_synset_rankings
import numpy as np


def predict_ranked_parents(embs, model, possible_parents, parents_to_synsets, words, postype):
    ids_to_synsets = load_id_to_synset_name(postype)
    
    word_embs = [embs.get_vector(x) for x in words]
    parent_embs = [x for x in zip(possible_parents, [embs.get_vector(x) for x in possible_parents]) if x[1] is not None]
    
    
    for i in range(len(words)):
        if word_embs[i] is None:
            continue
        adiffs = [embs.get_diff(parent_embs[j][1], word_embs[i]) for j in range(len(parent_embs))]
        preds = list(np.squeeze(model.predict(np.array(adiffs))))
        sorted_answers = sorted(zip(possible_answers, preds), key=lambda x: x[1], reverse=True)
        rankings = get_synset_rankings(sorted_answers, parents_to_synsets)
    
    for synset in closest:
        results.append((words[i], synset, id_name_dict[synset]))
    print(f"processed {i} out of {len(words)}")
        
        