import pandas as pd

table_paths = [f"resources/validation_24042020_N_m7_n1.csv", f"resources/validation_24042020_N_m7_n5.csv", f"resources/validation_24042020_V_m7_n1.csv",
               f"resources/validation_24042020_V_m7_n5.csv", f"resources/training_24042020_N_m7_n1.csv", f"resources/training_24042020_N_m7_n5.csv",
               f"resources/training_24042020_V_m7_n1.csv", f"resources/training_24042020_V_m7_n5.csv"]

for path in table_paths:
    table = pd.read_csv(path, index_col="Unnamed: 0", sep=',')
    table.to_csv(path, sep=",", index=False, header=False)

