from sklearn.ensemble import GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import classification_report, precision_recall_fscore_support
from sklearn.model_selection import GridSearchCV, train_test_split, StratifiedKFold
# from multiprocessing.dummy import Pool
from rwn_parser import POSTypes, RelTypes, build_ruthes_graph
from embedding_ops import ModelWrapper, ModelTypes, evaluate_vocab, evaluate_graph, find_worst_oov
from utils import Log, save_graph

# this one is just to evaluate how bad the sitch is with oov items

taiga = ModelWrapper(ModelTypes.W2V, "tayga_upos_skipgram_300_2_2019.bin")
rnc = ModelWrapper(ModelTypes.W2V, "ruwikiruscorpora_upos_skipgram_300_2_2019.bin")
superbig = ModelWrapper(ModelTypes.W2V, "ruwikiruscorpora_superbigrams_2_1_2.vec")

oov_taiga, all_taiga = evaluate_vocab(taiga, POSTypes.N, RelTypes.HYP)
oov_rnc, all_rnc = evaluate_vocab(rnc, POSTypes.N, RelTypes.HYP)
oov_sb, all_sb = evaluate_vocab(superbig, POSTypes.N, RelTypes.HYP)

out_str_t = f"OOV STATS FOR TAIGA: \n TOTAL OOV: {len(oov_taiga)} \n OOV PERCENTAGE: {len(oov_taiga) / float(len(all_taiga))}, total wordcount: {len(all_taiga)}"
out_str_r = f"OOV STATS FOR RNC: \n TOTAL OOV: {len(oov_rnc)} \n OOV PERCENTAGE: {len(oov_rnc) / float(len(all_rnc))}, total wordcount: {len(all_rnc)}"
out_str_s = f"OOV STATS FOR SUPERBIG: \n TOTAL OOV: {len(oov_sb)} \n OOV PERCENTAGE: {len(oov_sb) / float(len(all_sb))}, total wordcount: {len(all_sb)}"

logger = Log()
logger.write(out_str_t)
logger.write(out_str_r)
logger.write(out_str_s)
print(out_str_t)
print(out_str_r)
print(out_str_s)

graph = build_ruthes_graph(POSTypes.N)

biv, oiv, boov, total = evaluate_graph(superbig, graph, POSTypes.N)

out_str_graph = f"RuThes for postype N contains {total} relationships of which {biv} " \
                f"have both words in-vocabulary, {oiv} only have one, and {boov} have neither, for a proportion" \
                f" of {biv / float(total)} relationships fully in-vocabulary"

logger.write(out_str_graph)
print(out_str_graph)

worst_oov = find_worst_oov(superbig, graph, POSTypes.N)

out_str_worst = "The OOV items with the most outbound connections are the following: \n"

for i in range(10):
    out_str_worst += f"{worst_oov[i][0]} with {worst_oov[i][1]} outbound connections\n"

print(out_str_worst)
logger.write(out_str_worst)
